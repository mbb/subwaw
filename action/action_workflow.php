<?php

define("PATH","..");
define("PAGE","actionWorkflow");

session_start ();

require_once "../dao/DBquery.php";

//require_once "../inc/php/buildHeader.php";


$action = NULL;

if(isset($_POST['action'])) {
    $action = $_POST['action'];
}

function izrand($length = 16) {
    $random_string = "";
    while(strlen($random_string)<$length && $length > 0) {
        $randnum = mt_rand(0,61);
        $random_string .= ($randnum < 10) ?
            chr($randnum+48) : ($randnum < 36 ? 
                chr($randnum+55) : chr($randnum+61));
    }
    return $random_string;
}

function rrmdir($dir) { 
    if (is_dir($dir)) { 
      $objects = scandir($dir);
      foreach ($objects as $object) { 
        if ($object != "." && $object != "..") { 
          if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
            rrmdir($dir. DIRECTORY_SEPARATOR .$object);
          else
            unlink($dir. DIRECTORY_SEPARATOR .$object); 
        } 
      }
      rmdir($dir); 
    } 
  }

  function zipData($source, $destination) {
    if (extension_loaded('zip')) {
        if (file_exists($source)) {
            $zip = new ZipArchive();
            if ($zip->open($destination, ZIPARCHIVE::CREATE)) {
                $source = realpath($source);
                if (is_dir($source)) {
                    $iterator = new RecursiveDirectoryIterator($source);
                    // skip dot files while iterating 
                    $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);
                    $files = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);
                    foreach ($files as $file) {
                        $file = realpath($file);
                        if (is_dir($file)) {
                            $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                        } else if (is_file($file)) {
                            $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                        }
                    }
                } else if (is_file($source)) {
                    $zip->addFromString(basename($source), file_get_contents($source));
                }
            }
            return $zip->close();
        }
    }
    return false;
}

if($action == "download" || $action == "run") {

    $name = str_replace(' ', '_', $_POST['name']);
    $user = $_POST['user'];
    $passwd = $_POST['passwd'];


    $randDir = izrand();

    $path_output = Conf::$PATH_YAML_OUTPUTS . $randDir."/";
    $path_env_files =  Conf::$PATH_WAW . "/envs";
    $path_output2 = $path_output . $name . "/";


    $path_yaml = $path_output2 . $name.".yaml";
    $path_json = $path_output2 . $name.".json";

    // Save id and username in var and change right on directory
    $oldumask = umask(0);
    $user = Conf::$USER;
    mkdir($path_output, 0775, true);
    mkdir($path_output2, 0775, true);
    system("sudo chown -R $user $path_output");
    system("sudo chown -R $user $path_output2");
    umask($oldumask);


    $png = $_POST['png'];
    $data = $png;
    list($type, $data) = explode(';', $data);
    list(, $data)      = explode(',', $data);
    $data = base64_decode($data);
    $file = $path_output2.$name.".png";
    file_put_contents($file, $data);

    file_put_contents($path_yaml, $_POST['preview']);
    file_put_contents($path_json, $_POST['json']);
    $fileDebug = '/tmp/debug.txt';
    $cmd = "sudo -Eu " . $user . " bash -c \"PYTHONIOENCODING=utf-8 python3 " . Conf::$PATH_WAW . "generate_workflow_snakefile.py " . $name . " " . $path_output . " " . Conf::$PATH_WAW . " 2> /tmp/generate_wf_log \"";
    file_put_contents($fileDebug, $cmd."\n");

    system($cmd);
    $generateWFoutput = file_get_contents("/tmp/generate_wf_log");
    file_put_contents($fileDebug, $generateWFoutput."\n", FILE_APPEND | LOCK_EX);

    $path_snakefile = $path_output2 . $name.".snakefile";

    $path_sag = $path_output2 ."sag.yaml";

    //file_put_contents($path_sag, "");

    $cmd = "sudo -Eu " . $user . " bash -c \"PYTHONIOENCODING=utf-8 python3 " . Conf::$PATH_WAW . "generate_sag_yaml.py " . $path_yaml . " " . Conf::$PATH_WAW . " > " . $path_sag . " 2> /tmp/generate_wf_log \""; 
    file_put_contents($fileDebug, $cmd."\n", FILE_APPEND | LOCK_EX);

    system($cmd);
    $generateWFoutput = file_get_contents("/tmp/generate_wf_log");
    file_put_contents($fileDebug, $generateWFoutput."\n", FILE_APPEND | LOCK_EX);

    if (!is_dir('/tmp/dummy')) {
        mkdir("/tmp/dummy", 0777, true);
        system("sudo chown -R $user /tmp/dummy");
    }else{
        system("sudo chown -R $user /tmp/dummy");
    }

    $cmd = "sudo -Eu " . $user . " bash -c \"PYTHONIOENCODING=utf-8 python3 " . Conf::$PATH_WAW . "generate_workflow.py " . $path_yaml . " " . $path_output2 . " " . Conf::$PATH_WAW . " default > /tmp/generate_wf_log 2>&1 \"";

    $path_dockerfile = $path_output2 . "Dockerfile";
    file_put_contents($fileDebug, $cmd."\n", FILE_APPEND | LOCK_EX);

    system($cmd);
    $generateWFoutput = file_get_contents("/tmp/generate_wf_log");
    file_put_contents($fileDebug, $generateWFoutput."\n", FILE_APPEND | LOCK_EX);

    $oldumask = umask(0);    
    mkdir($path_output2."sagApp", 0777, true);
    umask($oldumask);

    $cmd = "sudo -Eu " . $user . " bash -c 'cd " . Conf::$PATH_SAG . " && Rscript main.R " . $path_sag . " " . $path_output2. "sagApp \"". $user."\" \"".$passwd."\" > /dev/null 2>&1 ' ";
    system($cmd);
    file_put_contents($fileDebug, $cmd."\n", FILE_APPEND | LOCK_EX);

    if($action == "download") {

        if(!class_exists('ZipArchive'))
            die("ZipArchive 3 NOT supported.");

        $filename = "/tmp/".rand(1,100000).".zip";

        zipData($path_output, $filename);

        rrmdir($path_output);
        
        ob_clean();

        if (file_exists($filename)) {
            ob_clean();
            header('Content-Type: application/zip');
            header('Content-Disposition: attachment; filename="'.$name.'.zip"');
            header('Content-Length: ' . filesize($filename));
            readfile($filename);
            unlink($filename);
        } 
        else {
            die("Zip not create");
        }
    }
    else if($action == "run" && Conf::$ACTIVATE_RUN == true) {

        //TODO install all packages
        // $path_dockerfile
        $host = '0.0.0.0';
        $port = '3838';
        $stdout = "ok";
        $stderr = "ok";
        


        // stop running Shiny app.
        $pidf=0;
        $pidfile = '/workflow/runingRscriptPID.txt';
        if (file_exists($pidfile))
        {
            $pidf = file_get_contents($pidfile);
        }

        if(isset($_SESSION['Rscriptprocess']) ) { 
            $pids = $_SESSION['Rscriptprocess']; 
        }
        if(isset($_SESSION['Rscriptprocess']) & file_exists('/proc/$pids)'))
        {
            //kill the process saved in current session and its children
            $cmd = "sudo pkill -9 -P ".$pids;
            shell_exec($cmd . "| tee -a /tmp/mylog 2>/dev/null >/dev/null ");

            //remove zombie process
            $cmd = "sudo kill -HUP $(sudo ps -A -ostat,ppid | grep -e '[zZ]'| awk '{ print $2 }')";
            shell_exec($cmd . "| tee -a /tmp/mylog 2>/dev/null >/dev/null ");
            sleep(3); 

        } elseif (file_exists($pidfile) & file_exists('/proc/$pidf)') ) {
            
            //kill the process created during an other session and its children
            $cmd = "sudo pkill -9 -P ".$pidf;
            shell_exec($cmd . "| tee -a /tmp/mylog 2>/dev/null >/dev/null ");
            unlink($pidfile);
            //remove zombie process
            $cmd = "sudo kill -HUP $(sudo ps -A -ostat,ppid | grep -e '[zZ]'| awk '{ print $2 }')";
            shell_exec($cmd . "| tee -a /tmp/mylog 2>/dev/null >/dev/null ");
            sleep(3);            
            file_put_contents("/tmp/mylog", $pidf." was killed\n", FILE_APPEND | LOCK_EX);

        }  else {
            //situations where no recorded pid, try to get one runing shiny process
            $stdout = "no process";
            $cmd = "sudo kill -HUP $(sudo ps aux | grep -e shiny | grep -v grep | awk '{ print $2 }')";
            shell_exec($cmd . "| tee -a /tmp/mylog 2>/dev/null >/dev/null ");
            sleep(3);           
            //remove zombie process
            $cmd = "sudo kill -HUP $(sudo ps -A -ostat,ppid | grep -e '[zZ]'| awk '{ print $2 }')";
            shell_exec($cmd . "| tee -a /tmp/mylog 2>/dev/null >/dev/null ");
            sleep(3);             
            file_put_contents("/tmp/mylog", $cmd ." used to find and killed the process\n", FILE_APPEND | LOCK_EX);
        }

        $connection = fsockopen($host, $port);
        if (is_resource($connection))
        {
           // echo '<h2>' . $host . ':' . $port . ' ' . '(' . getservbyport($port, 'tcp') . ') is open.</h2>' . "\n";
           $stderr = "busy";
            //fclose($connection);
        }
        else
        {
            $stderr = "cool";
            //echo '<h2>' . $host . ':' . $port . ' is not openresponding.</h2>' . "\n";

            $cmd = "sudo [ -d '/workflow' ] && sudo rm -f /workflow ";
            shell_exec($cmd . "| tee -a /tmp/mylog 2>/dev/null >/dev/null ");
            file_put_contents("/tmp/mylog", $cmd ." rm older /workflow dir \n", FILE_APPEND | LOCK_EX);

            $cmd = "sudo  ln -s -f ".$path_output2."/files /workflow";
            shell_exec($cmd . "| tee -a /tmp/mylog 2>/dev/null >/dev/null "); 
            file_put_contents("/tmp/mylog", $cmd ." cerate symlink to the workflow files \n", FILE_APPEND | LOCK_EX);

            $cmd = "sudo  ln -s -f ".$path_env_files." /workflow";
            shell_exec($cmd . "| tee -a /tmp/mylog 2>/dev/null >/dev/null "); 
            file_put_contents("/tmp/mylog", $cmd ." cerate symlink to environment files \n", FILE_APPEND | LOCK_EX);

            // stop running Shiny app.
            //$return_value = proc_close($process);
            
            $descriptorspec = array(
                0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
                1 => array("file", "/tmp/mylog.txt", "w"),  // stdout is a file to write to
                2 => array("file", "/tmp/mylog.txt", "a") // stderr is a file to write to
             );

            // run as root to avoid paths write access
            // however the path is not set as with root logging !!!?
            //$cmd = "sudo Rscript -e  \"Sys.setenv(PATH=paste0(Sys.getenv('PATH'),':/opt/biotools/bin'));setwd('".$path_output2."/sagApp/'); \" -e  \"shiny::runApp('".$path_output2."/sagApp/app.R',port=3838 , host='0.0.0.0')\"";
            $cmd = "sudo -Eu " . $user . " Rscript -e  \"setwd('".$path_output2."/sagApp/'); \" -e  \"shiny::runApp('".$path_output2."/sagApp/app.R',port=3838 , host='0.0.0.0')\"";
            file_put_contents($fileDebug, $cmd."\n", FILE_APPEND | LOCK_EX);
            //shell_exec($cmd . " 2>&1 | tee -a /tmp/mylog 2>/dev/null >/dev/null &");
            $process = proc_open($cmd, $descriptorspec, $pipes);
            $pid=proc_get_status($process)['pid'];
            $_SESSION['Rscriptprocess']= $pid;

            $pidfile = '/workflow/runingRscriptPID.txt';
            file_put_contents($pidfile, $pid, LOCK_EX);
            
        }
        //die();

        header('Content-Type: application/json');
        $json_ugly = '{"stdout": "'.$stdout.'","stderr": "'.$stderr .'"}';
        $json_pretty = json_encode(json_decode($json_ugly), JSON_PRETTY_PRINT);
        echo $json_pretty;
       
    }
    else {
        header("Location: ../workflow.php");
    }
}

