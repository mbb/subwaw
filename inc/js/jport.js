if(SUBWAW === undefined) {
    var SUBWAW = {};
}

SUBWAW.JPort = function(port) {

    this.name = port.name;
    this.type = port.type;
    this.value = port.value;
    this.description = port.description == undefined ? "" : port.description;
    this.ptype = port.ptype;
    this.from = port.from;
    this.list = port.list;

    this.initType = function(port) {

      this.type = port.type;

      if(this.type == "reads") {
        this.type += " / fq.gz";
      }
      else{
        var tmp = "";

        if(Array.isArray(this.type)) {
          for (var t = 0; t < (this.type.length); t++) {
            if(t == 0) {
              tmp += this.type[t]; 
            }
            else {
              tmp += " / " + this.type[t]; 
            }
          }

          this.type = tmp;
        }
      }
    }

  }