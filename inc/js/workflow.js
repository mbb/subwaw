
/**
 * Namespace for subwaw lib
 */
var SUBWAW = {};

/**
 * Excape quote & double quote from html string
 * 
 * @param {String} text
 * 
 * @return {String}
 */
function escapeHtml(text) {

    var map = {
      '"': '&quot;',
      "'": '&#039;'
    };

    return text.replace(/["']/g, function(m) { return map[m]; });
}

/**
 * Event when the window is resize.
 */
function reportWindowSize() {
    document.getElementById('myDiagramDivParent').style.width = (window.innerWidth - 517) + "px";
    document.getElementById('myDiagramDiv').style.width = (window.innerWidth - 517) + "px";
    document.getElementById('consoleRow').style.width = (window.innerWidth - 517) + "px";
    document.getElementById('consoleRow').style.height = (document.getElementById('toolsDiv').offsetHeight - 1002) + "px";
    document.getElementById('consolePreview').style.height = (document.getElementById('toolsDiv').offsetHeight - 1052) + "px";
}

/**
 * Sort Array with key 
 */
function getSortedKeys(obj) {
  var keys = Object.keys(obj);
  return keys.sort(function(a,b){return obj[a]-obj[b]});
}

/**
 * 
 * Get and show the YAML value of the tool with his name.
 * 
 * @param {string} tid The tid of the tool.
 */
function showYamlTool(tid) {

  if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
  } else {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
  }

  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          var value = "";
          var tool = JSON.parse(this.responseText);

          if(tool.length > 0) {
              value = tool[0]["yaml"];
          }

          document.getElementById('valueToolFile').value = value;
          var size = value.split(/\r\n|\r|\n/).length;
          document.getElementById("valueToolFile").rows = size; 
          $('#modalShowTool').modal();
      }
  };

  xmlhttp.open("GET","api/tools.php?tid="+tid,true);
  xmlhttp.send();

}

/**
 * 
 * Get and show the YAML value of the workflow with his name.
 * 
 * @param {string} name The name of the workflow.
 */
function showYamlWorkflow(name) {

  if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
  } else {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
  }

  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          var value = "";

          var workflow = JSON.parse(this.responseText);

          if(workflow.length > 0) {
              value = workflow[0]["yaml"];
          }

          document.getElementById('valueWorkflowFile').value = value;
          var size = value.split(/\r\n|\r|\n/).length;
          document.getElementById("valueWorkflowFile").rows = size; 
          $('#modalShowWorkflow').modal();
      }
  };

  xmlhttp.open("GET","api/workflows.php?name="+name,true);
  xmlhttp.send();

}

/**
 * Get a random number beetween min & max value.
 * 
 * @param {int} min
 * @param {int} max
 * 
 * @return {int}
 */
function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

/**
 * Search Tool or Data node
 * 
 * @param {string} templateID The id of search input
 * @param {bool} nodetype If true this is for Tool node search otherwise is for Data node
 */
function searchNodes(templateID, nodetype) {

    var MBB_NODE = [];

    if(nodetype == true) {
        MBB_NODE = MBB_TOOLS;
    }
    else {
        MBB_NODE = MBB_DATA;
    }

    var input, filter, paletteName;
    input = document.getElementById(templateID + "_searchInput_input");
    paletteName = templateID + "_searchInput";
    filter = input.value.toLowerCase();

    var node_name = [];
    var blanck_node = [];

    // get lowercase text value & ignore "blank" & "multiqc" node
    for (var key in MBB_NODE) {
        var palette =  MBB_NODE[key];
        for (let j = 0; j < palette.length; j++) {
            var node = palette[j];
            if(node.name != "blank" && node.name != "multiqc") {
                node_name.push(node.text.toLowerCase());
            }
            else {
                blanck_node.push(node);
            }
        }
    }

    node_name.sort();

    var node_name = node_name.filter(name => name.includes(filter));

    var nodes = [];

    nodes.push(blanck_node[0]);

    // you can make this search again because we need sort node name find 
    // with first loop
    for (var key in MBB_NODE) {
        var palette =  MBB_NODE[key];
        for (let j = 0; j < palette.length; j++) {
            var node = palette[j];
            if(node.name != "blank") {
                if(node_name.includes(node.text.toLowerCase())) {
                    nodes.push(node);
                }
            }
        }
    }

    document.getElementById(templateID + "_searchSpan").textContent = nodes.length - 1;
    resetPalette(paletteName);
    myPalette_node = createPalette(paletteName, nodes);
}

/**
 * 
 * Event when user import JSON file
 * 
 * @param {Object} element 
 */
function handleJsonFiles(element) {

    var file = element.files[0];

    if(file.size == 0) {
      alert("Error: Your JSON file is empty.");
    }
    else if(file.type != "application/json") {
      alert("Error: Your file is not in JSON format.");
    }
    else {
      document.getElementById("formImportJSON").submit();
    }
  }