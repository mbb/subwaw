
<?php

$defaultPath = ".";
if (defined('PAGE')){
if(PAGE == "actionWorkflow") {
    $defaultPath = PATH;
}
}
echo '
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.0/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" crossorigin="anonymous">

    <!-- local fallbacks -->
    <!--link rel="stylesheet" href="'.$defaultPath.'/inc/css/bootstrap.min.css">
    <link rel="stylesheet" href="'.$defaultPath.'/inc/css/jquery.dataTables.min.css"-->

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css"></link>

    <link rel="stylesheet" href="'.$defaultPath.'/inc/css/style.css">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <script src="'.$defaultPath.'/inc/js/jscolor.js"></script>
    <title>subwaw</title>

</head>
<body>


<nav class="navbar navbar-expand-md navbar-dark navbar-custom p-1">
    <a class="navbar-brand" href="'.$defaultPath.'/index.php">SUBWAW</a>
    <div class="collapse navbar-collapse justify-content-between" id="navbar">
        <div class="navbar-nav">
        ';


            //echo ' <a class="nav-item nav-link" href="./index.php"> Home </a>';

            //echo ' <a class="nav-item nav-link" href="./wrapper.php"> Wrapper </a>';

            //echo ' <a class="nav-item nav-link" href="./brick.php"> Tool </a>';

            echo ' <a class="nav-item nav-link" href="'.$defaultPath.'/toolmanager.php"> Tools Manager </a>';

            //echo ' <a class="nav-item nav-link" href="./workflow.php"> Workflow </a>';

            echo ' <a class="nav-item nav-link" href="'.$defaultPath.'/workflowmanager.php"> Workflows Manager </a>';


            if(isset($_SESSION['manage'])) {
                echo ' <a class="nav-item nav-link" href="'.$defaultPath.'/manage.php"> Manage </a>';
            }

        echo '
        </div>
        <div class="navbar-nav">
            ';
            if(isset($_SESSION['username']) && !empty($_SESSION['username']))
            {
                echo '<a class="nav-item btn btn-primary " href="'.$defaultPath.'/action/action_logout.php"> Logout ('.$_SESSION['username'].') </a>';
            }
            else
            {
                echo '<a class="nav-item btn btn-primary" href="'.$defaultPath.'/login.php"> Login </a>';
            }
           
        echo '</div></div>

</nav>

';

if (defined('PAGE')){
if(PAGE == "PAGE") {
    echo '<br/>';
}
}