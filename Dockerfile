FROM mbbteam/mbb_workflows_base:latest as alltools
RUN apt-get update


# # Copy this repo into place.
# VOLUME ["/var/www", "/etc/apache2/sites-enabled"]

# # Update the default apache site with the config we created.
# ADD apache-config.conf /etc/apache2/sites-enabled/000-default.conf

# RUN echo "ServerName 12.0.1" >> /etc/apache2/sites-enabled/000-default.conf

WORKDIR /var/www/html


#RUN chown -R www-data:www-data  /var/www/html
#RUN chmod -R +w  /var/www/html/waw/output




RUN cd /opt/biotools \
 && GIT_SSL_NO_VERIFY=true git clone https://gitlab.mbb.univ-montp2.fr/khalid/vcfparser.git



#Pour pouvoir lancer l'appli. shiny en root
RUN apt-get update && apt-get install sudo \
&& usermod -aG sudo www-data \
&& adduser www-data sudo \
&& echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers



RUN apt-get update && apt-get install -y python3-qtpy python3-numpy python3-pyqt5 python3-pyqt5.qtopengl python3-lxml python3-six xvfb
RUN pip3 install  biopython==1.69



RUN apt-get update \
 && apt-get install -y openjdk-8-jdk \
 && apt-get install  -y ant

ENV PATH=${PATH}:/opt/biotools/jmodeltest-2.1.10
ENV JMODELTEST_HOME /opt/biotools/jmodeltest-2.1.10
RUN cd /opt/biotools \
 && wget https://github.com/ddarriba/jmodeltest2/files/157117/jmodeltest-2.1.10.tar.gz \
 && tar -xvzf jmodeltest-2.1.10.tar.gz \
 && rm jmodeltest-2.1.10.tar.gz \
 && cd $JMODELTEST_HOME 



RUN apt -y update && apt install -y tabix python3-matplotlib imagemagick

#RUN apt -y update && apt-get install -y python-backports.functools-lru-cache
#RUN pip install backports.functools-lru-cache



RUN apt-get update\
    && apt-get install -y gcc zlib1g-dev pkg-config libfreetype6-dev libpng-dev
RUN cd /opt/biotools \
 && GIT_SSL_NO_VERIFY=true git clone https://gitlab.mbb.univ-montp2.fr/khalid/radsex \
 && cd radsex\
 && make -j 10 \
 && echo 'PATH=$PATH:/opt/biotools/radsex/bin' >> /etc/environment
# Need to install in conda env rgtools, r-reshape2 and r-sgtr
# && mamba install -y -c r -c bioconda r-gtools r-reshape2 \
# && Rscript -e "devtools::install_github('SexGenomicsToolkit/sgtr');library('sgtr')" \


ENV PATH $PATH:/opt/biotools/radsex/bin
# To restore !!

RUN apt-get update\
    && apt-get install -y gnuplot \
    && cd /opt/biotools 



RUN apt-get install -y libsodium-dev
RUN Rscript -e 'install.packages("shinyauthr",repos="https://cloud.r-project.org/",Ncpus=8, clean=TRUE)'
#Usefull to get tooltips
RUN Rscript -e 'install.packages("shinyBS",repos="https://cloud.r-project.org/",Ncpus=8, clean=TRUE)'


RUN pip3 install spython \
&& apt-get install -y ca-certificates


RUN apt-get install -y bioperl  circos \
&&  cd /opt/biotools && wget http://circos.ca/distribution/circos-current.tgz \
&& tar -xzf circos-current.tgz \
&& rm -rf circos-current.tgz \
&& mv circos* circos_current \
&& sed -i 's/max_points_per_track.*/max_points_per_track = 40000/' /opt/biotools/circos_current/etc/housekeeping.conf

ENV PATH /opt/biotools/circos_current/bin:$PATH 
RUN echo PATH=/opt/biotools/circos_current/bin:$PATH >> /etc/environment



RUN apt-get install -y ncbi-tools-bin infernal 


RUN cd /opt/biotools \
&& git clone https://github.com/raja-appuswamy/accel-align-release.git \
&& apt-get install -y libtbb-dev \
&& cd /opt/biotools \
&& git clone https://github.com/amplab/snap.git \
&& cd snap \
&& make -j 8 



RUN apt-get update && apt-get install -y libcairo2-dev libxt-dev \
&& apt-get  -y install libcurl4-openssl-dev libssl-dev libharfbuzz-dev libfribidi-dev libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev



# force redirect from http://127.0.0.1/ to http://127.0.0.1/subwaw 
RUN echo '<meta http-equiv="refresh" content="0; url=/subwaw/" />' > /var/www/html/index.html



 RUN  cd /opt/biotools \
 &&   git clone https://github.com/BELKHIR/vcfmultisampleparser.git





RUN echo '<Directory /var/www/html/>' >> /etc/apache2/apache2.conf \
&& echo '    Options Indexes FollowSymLinks' >> /etc/apache2/apache2.conf \
&& echo '    AllowOverride All' >> /etc/apache2/apache2.conf \
&& echo '    Require all granted' >> /etc/apache2/apache2.conf \
&& echo '</Directory>'  >> /etc/apache2/apache2.conf \
&& echo  'AuthType Basic'  > /var/www/html/.htaccess \
&& echo  'AuthType Basic' >> /var/www/html/.htaccess \
&& echo  'AuthName "Restricted Content"' >> /var/www/html/.htaccess \
&& echo  'AuthUserFile /var/www/html/subwaw/.htpasswd' >> /var/www/html/.htaccess \
&& echo  'Require valid-user' >> /var/www/html/.htaccess 


ENV PATH /opt/biotools/conda/bin:$PATH
ENV PATH /opt/biotools/conda/condabin:$PATH

RUN echo  PATH=/opt/biotools/accel-align-release:/opt/biotools/bin:/opt/biotools/snap:/opt/biotools/circos_current/bin:$PATH >> /etc/environment
RUN echo AUGUSTUS_CONFIG_PATH=/usr/share/augustus/config >> /etc/environment \
&& echo JMODELTEST_HOME=/opt/biotools/jmodeltest-2.1.10 >> /etc/environment
#&& echo BUSCO_CONFIG_FILE=/opt/biotools/busco_config.ini >> /etc/environment

RUN sed -i s'|secure_path="|secure_path="/opt/biotools/conda/bin:|'  /etc/sudoers
# Expose apache.
EXPOSE 80

# By default start up apache in the foreground, override with /bin/bash for interative.
CMD /usr/sbin/apache2ctl -D FOREGROUND

