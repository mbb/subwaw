<?php

session_start ();

define("PAGE","CREATE_WORKFLOW");

require_once("./inc/php/buildHeader.php");

require_once("./dao/DBquery.php");

require_once("./tool.php");

$db = new DBqueryLite();


$import = false;

if(isset($_GET['workflowid']) && isset($_GET['action'])) {

  if($_GET['action'] == "RUN" ) {
    $run = true;
    if ($_GET['workflowid'] != "")
    {
    $jsonFile = Conf::$PATH_YAML_WORKFLOWS . $_GET['workflowid'] . "/" . $_GET['workflowid'] . ".json";
    $jsonString = file_get_contents($jsonFile);
    $pngFile = "../waw/workflows/" . $_GET['workflowid'] . "/" . $_GET['workflowid'] . ".png";
    echo '<p style="text-align:center;"><img src="' . $pngFile . '" /></p>';
    // Get the image content
    $image = file_get_contents($pngFile);

    // Convert it to base64
    $base64_image = "data:image/png;base64,". base64_encode($image);
    }
    else
    {
      $jsonString=$_POST['jsonWorkFlow'];
    }
  }

  if($_GET['action'] == "save" ) {
    $yaml = ($_GET['workflowid']) or exit("Unable to open file!"); 

    $org = Conf::$PATH_YAML_WORKFLOWS . $_GET['workflowid'] . "/" . $_GET['workflowid'] . ".yaml";

    $copy = $org . date("-m-d-Y-His");
    copy($org,$copy);

    if (!$f = fopen($org, 'w')) {
        echo "Impossible d'ouvrir le fichier ($ork)";
        exit;
    }
    fwrite($f, $_POST['save']);
    fclose($f);

  }
 
	if($_GET['action'] == "preview" ) {
	  $yamlText = $_POST['yamlText'];
	
    echo '<p style="text-align:center;"><img src="' . $_POST['helperpng'] . '" /></p>';
    if ($_GET['workflowid'] != ""  && $_POST['jsonWorkFlow'] == "")
    {
    $jsonFile = Conf::$PATH_YAML_WORKFLOWS . $_GET['workflowid'] . "/" . $_GET['workflowid'] . ".json";
    $jsonString = file_get_contents($jsonFile);
    }
    else
    {
      $jsonString=$_POST['jsonWorkFlow'];
    }
  }
}
?>


<div class="container-fluid" >
    <div class="row"  >
    <div id="sample">
    <!-- <div style="width: 100%; display: flex; justify-content: space-between">-->
    <div  style="width: 100%; padding-left: 5px; margin: 5px">
      <!--<button type="button" id="previewButton" class="btn btn-dark btn-sm"  data-toggle="modal" data-target="#prefiewYAML" onclick="generateWorkflow(true, false)"><i class="fas fa-eye"></i>&nbsp;Preview config file</button>-->
      <button  type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#downloadProject" ><i class="fas fa-file-archive"></i>&nbsp;Download</button>
      <button  type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#runProject" onclick="hideSpinner();" ><i class="fas fa-play"></i>&nbsp;Run</button>
    </div>
    <div id="toolsDiv" style="width: 100%; display: flex; justify-content: space-between">
      <div id="infoBoxHolder"></div>
    </div>
  </div>
  </div>
  
  <br/>

  <?php 

drawImportModal("importJSON", "Import from JSON");

drawPreviewModal("prefiewYAML", "Preview config file");

drawDownloadModal("downloadProject", "Download workflow");

if(Conf::$ACTIVATE_RUN) {
  drawRunModal("runProject", "Run workflow", "workflow_".rand(10000,99999));
  drawRunSuccessModal("runSuccessModal", "Running workflow");
}

?>
</div>


<script src="./inc/js/node_modules/gojs/assets/js/jquery.min.js"></script>
<script src="./inc/js/node_modules/gojs/assets/js/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script src="./inc/js/node_modules/gojs/release/go.js"  ></script>
<script src="./inc/js/Figures.js"  ></script>
<script src="./inc/js/workflow.js"></script>
<script src="./inc/js/jtool.js"></script>
<script src="./inc/js/jport.js"></script>
<script src="./inc/js/jtoolinfobox.js"></script>
<!--
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
-->   
<script>

function hideSpinner() {
      document.getElementById("nameWRun").value = "workflow_"+getRandomArbitrary(10000, 99999);
      $("#runSpinner").hide();
      $("#runButton").prop("disabled", false);
    }

function printToConsole(msg, type = MESSAGE_TYPE.DEFAULT) {
      document.getElementById("consolePreview").innerHTML += "<p style='color: " + type + "' > " + msg + "</p><br/>";
    }

const unique = (value, index, self) => {
  return self.indexOf(value) === index
}

function exportJSON(linkElement) {
      saveDiagramProperties();  // do this first, before writing to JSON
      var json = myDiagram.model.toJson();
      var objjson = JSON.parse(json);
      objjson.version = <?php echo Conf::$VERSION; ?>;
      myDiagram.isModified = false;
      var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(objjson));
      linkElement.setAttribute("href", "data:"+data);
    }

function downloadWorkflow() {
      
      var previewWorkflow = generateWorkflow(false, false);
      
      document.getElementById("pngWorkflow").value = <?php if (isset($_POST['helperpng']) )  {echo '"'.$_POST['helperpng'].'";';} else {echo "'".$base64_image."';";} ?>;
      
      if(previewWorkflow) {
        //console.log("generate ok");
        //$('#'+id).modal('hide');
        // var json = myDiagram.model.toJson();
        // var objjson = JSON.parse(json);
        // objjson.version = <?php echo Conf::$VERSION; ?>;
        // document.getElementById("jsonWorkflow").value = JSON.stringify(objjson);
        var objjson = <?php echo $jsonString;  ?>;
        //objjson.version = <?php echo Conf::$VERSION; ?>;
        document.getElementById("jsonWorkflow").value = JSON.stringify(objjson);
        
        //document.getElementById("jsonWorkflow").value = "dummy";

        document.getElementById("formWorkflow").submit();
      }
    }

    

function showSpinner() {
      $("#runSpinner").show();
      $("#runButton").prop("disabled", true);
    }

function runWorkflow() {

      var previewWorkflow = generateWorkflow(false, true);

      if(previewWorkflow) {

        // var json = myDiagram.model.toJson();
        // var objjson = JSON.parse(json);
        // objjson.version = <?php echo Conf::$VERSION; ?>;
        $json = <?php echo $jsonString ; ?> ;
        document.getElementById("jsonWorkflowRun").value = JSON.stringify($json) ;
        document.getElementById("pngWorkflowRun").value = <?php if (isset($_POST['helperpng']) )  {echo '"'.$_POST['helperpng'].'";';} else {echo '"'.$base64_image.'";';} ?>;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
        }

        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var value = JSON.parse(this.responseText);
                //document.getElementById("mySavedModel").value = value;
                console.log(this.responseText);
                $('#runProject').modal('toggle');

                var message = "";
                var publicIP = "127.0.0.1";
                var shiny_port = "<?php echo Conf::$SHINY_PORT ; ?>";
                if(value["stderr"] == "cool") {
                 publicIP="<?php $IP=shell_exec("wget -qO- https://ipinfo.io/ip"); $IP = str_replace(["\n", "\r"], "", $IP); echo $IP; ?>";
                 message = "Your workflow is now running <a href = 'http://" + publicIP + ":" + shiny_port + "/' target='_blank'>here</a>.";
                }
                else {
                  message = "Your have to stop a running workflow to free the port 3838 before.";
                }

                document.getElementById("workflowRunning").innerHTML = message;
                $('#runSuccessModal').modal();
                
            }
        };

        var params = "";

        var name = document.getElementById("nameWRun").value;
        var description = document.getElementById("descriptionWRun").value;
        var version = document.getElementById("versionWRun").value;
        var author = document.getElementById("authorWRun").value;
        var png = document.getElementById("pngWorkflowRun").value;
        var preview = document.getElementById("previewWorkflowRun").value;
        var json = document.getElementById("jsonWorkflowRun").value;

        var user = document.getElementById("userWRun").value;
        var passwd = document.getElementById("passWRun").value;

        params += "action=run&";
        params += "name="+name+"&";
        params += "description="+description+"&";
        params += "version="+version+"&";
        params += "author="+author+"&";
        params += "png="+png+"&";
        params += "preview="+preview+"&";
        params += "json="+json+"&";
        params += "user="+user+"&";
        params += "passwd="+passwd;

        xmlhttp.open("POST","action/action_workflow.php", true);
        xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xmlhttp.send(params);
        showSpinner();
      }
    }

function generateWorkflow(preview, run) {
      //var content = transform3(preview, run);
      
   	  $path_yaml = <?php echo '"'.Conf::$PATH_YAML_WORKFLOWS . $_GET['workflowid'] . "/" . $_GET['workflowid'] . ".yaml".'"' ?> ;
	     
      textYaml = document.forms['yamltextarea']['save'].value;
      

      $data = textYaml;
      var content = $data;

      if(content != "") {
        if(preview) {
          document.getElementById("workflowPreview").value = content;
        }
        else {
          if(run) {
            document.getElementById("previewWorkflowRun").value = content;
          }
          else {
            document.getElementById("previewWorkflow").value = content;
          }
        }
        return true;
      }
      else {
        document.getElementById("console-tab").click();
        document.getElementById("console-tab").focus();
        var consoleOutput = document.getElementById("consolePreview");
        consoleOutput.scrollTop = consoleOutput.scrollHeight;
        return false;
      }
    }

</script>
<script>
    // Button to save edit
    var TR;
    function saveEdits() {
       var table, tr, editElem, userVersion = [], i, j;
       table    = document.getElementById("steps");
       tr       = table.getElementsByClassName("output"); //element
    
       TR = tr;
       //console.log(tr);
       values=document.forms['yamltextarea']['save'].value;
       for (i=0; i< tr.length; i++){
              // This will set a key value pair where key as row number and value as the inner HTML
              // This key will further help us updating the values from localhost
              replacement = tr[i].getElementsByTagName("td")[2].innerText;
              replacement = replacement.replaceAll(/[^A-Za-z0-9_]/g,''); //remove potential non alphanum char, 
              userVersion[tr[i].sectionRowIndex] = replacement;

			  search_expression = new RegExp('step_name:\\s+"' + tr[i].getElementsByTagName("td")[1].innerHTML + '"',"g" );
              //console.log(search_expression+" -> "+replacement);
              values = values.replaceAll(search_expression, 'step_name: "'+replacement+'"'); 

			  search_expression = new RegExp('title:\\s+"' + tr[i].getElementsByTagName("td")[1].innerHTML + '"',"g" );
              values = values.replaceAll(search_expression, 'title: "'+replacement+'"');

			  search_expression = new RegExp('origin_step:\\s+' + tr[i].getElementsByTagName("td")[1].innerHTML + '}',"g" );
              values = values.replaceAll(search_expression, 'origin_step: '+replacement+'}');

			  search_expression = new RegExp(' name:\\s+"' + tr[i].getElementsByTagName("td")[1].innerHTML + '"',"g" );
              values = values.replaceAll(search_expression, ' name: "'+replacement+'"');

        search_expression = new RegExp('param_A:\\s+"' + tr[i].getElementsByTagName("td")[1].innerHTML + '"',"g" );
              values = values.replaceAll(search_expression, 'param_A: "'+replacement+'"');

        search_expression = new RegExp('param_B:\\s+"' + tr[i].getElementsByTagName("td")[1].innerHTML + '"',"g" );
              values = values.replaceAll(search_expression, 'param_B: "'+replacement+'"');

        search_expression = new RegExp('remove:\\s+' + tr[i].getElementsByTagName("td")[1].innerHTML +'}',"g" );
              values = values.replaceAll(search_expression, 'remove: '+replacement+'}');
              //console.log(values);
        }  
        document.forms['yamltextarea']['save'].value = values;

       //console.log(userVersion);
       localStorage.setItem('userEdits', JSON.stringify(userVersion));
    }
    
    // place in the body to reload the changes
    function checkEdits() {
        try{
          var userEdits = localStorage.getItem('userEdits');
          //console.log(JSON.parse(userEdits));
          if (userEdits) {
              userEdits = JSON.parse(userEdits);
              table    = document.getElementById("mytable");
              tr       = table.getElementsByClassName("output"); //element
    
              for ( var elementId in userEdits ) {
                  tr[elementId].getElementsByTagName("td")[3].innerHTML = userEdits[elementId];
              }
          }
        }catch{
          //console.log("Hello");
        }
     }
</script>

<?php 
    function varexport($expression, $return=FALSE) {
		$export = var_export($expression, TRUE);
		$patterns = [
			"/array \(/" => '[',
			"/^([ ]*)\)(,?)$/m" => '$1]$2',
			"/=>[ ]?\n[ ]+\[/" => '=> [',
			"/([ ]*)(\'[^\']+\') => ([\[\'])/" => '$1$2 => $3',
			"/\n/" => '<br />',
			"/ /" => ' &nbsp; ',
			"/'{/" => '{ ',
			"/'$/" => ' ',
			
		];
		$export = preg_replace(array_keys($patterns), array_values($patterns), $export);
		if ((bool)$return) return $export; else echo $export;
	}  
   
	echo '<style type="text/css" >
	table,
	td {
	  border: 1px solid #333;
	  vertical-align: top;
	}
	
	thead,
	tfoot {
	  background-color: #333;
	  color: #fff;
	}
	
	</style>';
	
	

	//varexport(file_get_contents(Conf::$PATH_YAML_WORKFLOWS . $_GET['workflowid'] . "/" . $_GET['workflowid'] . ".yaml"));
	$file= "";
	if(isset($_GET['workflowid']) && $_GET['action'] == "RUN" && $_GET['workflowid'] != "") 
	{
		$file = Conf::$PATH_YAML_WORKFLOWS . $_GET['workflowid'] . "/" . $_GET['workflowid'] . ".yaml";
		$array = yaml_parse_file($file);
	}
	else
	{
    $chaineYAML =  $yamlText; 
    $array = yaml_parse( $chaineYAML);
	}
	
	
	//print_r($array["steps_in"])."</br>";
	$nbsteps = count($array["steps_in"]);
	echo "<input id='saveComment' type='button' value='Apply new names to WF config' onclick='saveEdits()'>";
	
	echo '
	<table><tr>
	<td>
	<table style="display:inline;" id="steps" class="table table-condensed table-hover table-striped table-sm bootgrid-table" width="60%" cellspacing="0">
    <thead>
      <tr>
         <th>step : tool</th>
         <th>step name</th>
         <th>New name</th>
      </tr>
   </thead>
   <tbody>';

    
	for ($i = 0; $i < $nbsteps; $i++) 
	{
		$stpnum = $i+1;
		echo '<tr class="output" data-row-id="'. $stpnum .'">';
		echo '<td contenteditable="false">' .$array["steps_in"][$i]["step_name"] ." : " . $array["steps_in"][$i]["tool_name"].' </td>';
		echo '<td contenteditable="faslse">'. $array["steps_in"][$i]["step_name"] .'</td>';
		echo '<td class="table-dark" contenteditable="true">'. $array["steps_in"][$i]["step_name"] . '</td>';
		echo "</tr>";
	}
    echo '</tbody>    </table>
	</td>';

    

	echo '<td><form style="display:inline;" name="yamltextarea" action="./runworkflow.php?workflowid='. $_GET['workflowid']. '&action=save"  method="post" >';
	echo '<P><INPUT TYPE="RESET" value="Reset WF config"></P>';
	echo '<TEXTAREA readonly NAME="save" COLS=120 ROWS=50 wrap="off"> ';
	if ($file != "")
	{include($file);}
    else
	{echo $yamlText;}	

	echo '</textarea>';
	
	echo '</FORM></td>';
	echo '</tr></table>';

	

?>

</body>
</html>
