<?php


require_once "../dao/DBquery.php";
require_once "../tool.php";

$name = "";

if(isset($_GET['name'])) {
    $name = $_GET['name'];
}


$workflows = getJSONWorkflows($name);

header('Content-Type: application/json');
$json_pretty = json_encode($workflows, JSON_PRETTY_PRINT);
echo $json_pretty;

?>