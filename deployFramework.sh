#!/usr/bin/env bash

# SUBWAW deployment script

## Installation
# Need docker (https://docs.docker.com/engine/install/ubuntu/)
# ./deployFramework.sh -i frameworkDir -d datadir -r resultdir -s dockerhub -n 80 -w 90 -p passwd --nbcpus 20 --memory 20

# A noter que le nombre de cpus visible à l'intérieur du container est toujours = au nombre total de cpus de la machine !

# for debug
#set -x

### Define parameter for script

usage="\n
$0 \n
\t\t--instdir|-i <install (framework) directory> [default to ${HOME}/MBBworkflow]\n
\t\t--datadir|-d <input/data directory> [default to ${HOME}/MBBworkflow/Data]\n
\t\t--resultdir|-r <output/result directory> [default to ${HOME}/MBBworkflowResults]\n
\t\t--imgsrc|-s <local|dockerhub> (docker image source) [default dockerhub]\n
\t\t--subwawport|-n <port> (port to use for the port 80 of the container = subwaw) [default 80]\n
\t\t--shinyport|-w <port> (port to use for the port 3838 of the container = sag/shiny) [default 90]\n
\t\t--password|-p <password> [default admin]\n
\t\t--nbcpus [default all available] \n
\t\t--memory [default all available] (in giga) \n\n
Example : $0 -i ${HOME}/MBBworkflow -d ${HOME}/MBBworkflow/Data -r ${HOME}/MBBworkflow/Results -s dockerhub -n 8080 -w 9090 -p mypasswd --nbcpus 20 --memory 20\n"

while [[ $# -gt 0 ]]
do
  opt="$1";
  case $opt in
    "-i"|"--instdir")
      installdir=${2}
      shift
      ;;
    "-d"|"--datadir")
      datadir=${2}
      shift
      ;;
    "-r"|"--resultdir")
      resultdir=${2}
      shift
      ;;
    "-s"|"--imgsrc")
      imgsrc=${2}
      shift
      ;;
    "-n"|"--subwawport")
      subwaw_port=${2}
      shift
      ;;
    "-w"|"--shinyport")
      shiny_port=${2}
      shift
      ;;
    "-p"|"--password")
      password=${2}
      shift
      ;;
    "--nbcpus")
      nbcpus="--cpus="${2}
      shift
      ;;
    "--memory")
      memory="-m "${2}"g"
      shift
      ;;
    *|"-h"|"--help")
      echo -e ${usage}
      exit 1
      ;;
  esac
  shift    #expose next argument
done


### Define port for sag and subwaw

echo -e "1- Define port for container :\n"

# subwaw port
if [ -z "${subwaw_port}" ]; then
  subwaw_port="80"
fi

if ! command -v netstat &> /dev/null
then
    echo "Netstat could not be found, so cannot verify if ${subwaw_port} port for subwaw is free"
else
    subwaw_ok=$(netstat -ltnp | grep -w ":${subwaw_port}")
    if [ -z "${subwaw_ok}" ]; then
      echo "The ${subwaw_port} port for subwaw is free, can continue !"
    else
      echo "The ${subwaw_port} port for subwaw is not free, you can see what use this port by running netstat -ltnp |grep -w ':${subwaw_port}' !"
    fi
fi

echo -e "- The application to build and define the workflow (subwaw) will be available on port ${subwaw_port}.\n"

# sag port
if [ -z "${shiny_port}" ]; then
  shiny_port="90"
fi

if ! command -v netstat &> /dev/null
then
    echo "Netstat could not be found, so cannot verify if ${shiny_port} port for sag (shiny) is free"
else
    shiny_ok=$(netstat -ltnp | grep -w ":${shiny_port}")
    if [ -z "${shiny_ok}" ]; then
      echo "The ${shiny_port} port for sag (shiny) is free, can continue !"
    else
      echo "The ${shiny_port} port for sag (shiny) is not free, you can see what use this port by running : netstat -ltnp |grep -w ':${shiny_port}' !"
      exit 1
    fi
fi

echo -e "- The application to set workflow parameter and to execute the workflow (sag) will be available on port ${shiny_port}.\n\n"


# Define directory for installation, data and results

echo -e "2- Define user and directory mount for container (install directory, download directory, data directory and results directory) :\n"

# user

if [ -z "${user}" ]; then
  user=${SUDO_USER:-$USER}
  uid=$(id -u $user)
  echo -e "- Define home directory and user for the container with login = ${user} and id = ${uid}"
fi

# Install
home=${SUDO_HOME:-$HOME}
if [ -z "${installdir}" ]; then
  installdir=${home}"/MBBworkflow"
fi

# Directory with waw, subwaw and sag
if [ ! -d "${installdir}/mbb-framework" ]; then
    mkdir -p ${installdir}/mbb-framework
fi

# Directory where download done by mbb framework will be saved
if [ ! -d "${installdir}/mbb-download" ]; then
    mkdir -p ${installdir}/mbb-download
fi

echo -e "- Install directory on machine is in ${installdir}/mbb-framework and it will be mount in the container in /var/www/html/"
echo -e "- Download directory on machine is in ${installdir}/mbb-download and it will be mount in the container in /Download/. This directory will contain all files downloaded by mbb workflow runs."

# Data

if [ -z "${datadir}" ]; then
  datadir=${home}"/MBBworkflow/Data"
fi
CREATE_DATA=0
if [ ! -d ${datadir} ] && [ ! -L ${datadir} ]; then
    mkdir -p ${datadir}/Databases
    CREATE_DATA=1
fi

echo -e "- Data directory on machine is in ${datadir} and it will be mount in the container in /Data/. A Databases sub-directory was created in ${datadir} to store all your personnal databases (possible to store them by symbolic link)."

# Results

if [ -z "${resultdir}" ]; then
  resultdir=${home}"/MBBworkflow/Results"
fi
CREATE_RESULTS=0
if [ ! -d ${resultdir} ] && [ ! -L ${resultdir} ]; then
    mkdir -p ${resultdir}
    CREATE_RESULTS=1
fi

echo -e "- Results directory on machine is in ${resultdir} and it will be mount in the container in /Results/\n\n"


# Define other parameter

if [ -z "${imgsrc}" ]; then
  imgsrc="dockerhub"
fi
if [ -z "${password}" ]; then
  password="admin"
fi
if [ -z "${nbcpus}" ]; then
  nbcpus="--cpus="$(nproc --all)
fi

if [ -z "${memory}" ]; then
  memory=""
fi

cd ${installdir}/mbb-framework


## cloning 3 repositories (waw, subwaw, sag)

echo -e "3- Cloning git directory in ${installdir}/mbb-framework : \n"

# waw
if [ ! -d "${installdir}/mbb-framework/waw" ];then
  git clone https://gitlab.mbb.univ-montp2.fr/mbb/waw.git
else
  cd waw
  git pull
  cd ..
fi

echo -e "\n"

# sag
if [ ! -d "${installdir}/mbb-framework/sag" ];then
  git clone https://gitlab.mbb.univ-montp2.fr/mbb/sag.git
else
  cd sag
  git pull
  cd ..
fi

echo -e "\n"

# subwaw
if [ ! -d "${installdir}/mbb-framework/subwaw" ];then
  git clone https://gitlab.mbb.univ-montp2.fr/mbb/subwaw.git
  cd subwaw
else
  cd subwaw
  git pull
fi

# add a rep that will contain conda generated environments
mkdir -p ${installdir}/mbb-framework/condabin


echo -e "\n\n4- Installation of gojs for web application : \n"

## GoJS library licensing.
echo -e 'You need to get a copy of GoJS (JavaScript diagramming library) after accepting The GoJS software license terms at https://gojs.net/latest/license.html  !\n'

if [ ! -d "${installdir}/mbb-framework/subwaw/inc/js/node_modules/gojs" ]; then
  mkdir -p "${installdir}/mbb-framework/subwaw/inc/js/node_modules/"
  cd "${installdir}/mbb-framework/subwaw/inc/js/node_modules/"
  wget https://github.com/NorthwoodsSoftware/GoJS/archive/refs/tags/v2.1.2.tar.gz
  if [ $? -eq 0 ]; then
    tar -zxvf v2.1.2.tar.gz
    mv GoJS-2.1.2 gojs
    rm -f v2.1.2.tar.gz
  else
    echo Download of GoJS FAILED !!!!
    exit 1
  fi
fi

cd ${installdir}/mbb-framework/subwaw

## create a sample PHP config file

echo -e "\n\n5- Create a php config file for web application.\n\n"

cat << EOF > conf/Conf.php
<?php

class Conf {

    public static \$VERSION = 2;

    public static \$DB_NAME       = 'subwaw';
    public static \$DB_HOSTNAME   = 'localhost';
    public static \$DB_USERNAME   = 'root';
    public static \$DB_PP         = 'mbb';
    public static \$SUBWAW_PORT   = '${subwaw_port}';
    public static \$SHINY_PORT    = '${shiny_port}';

    public static \$USER    = '${user}';

    public static \$KK = "00a0f54bf0b225204124985243c5390baa9d8bf2";

    public static \$PATH_WAW = "/var/www/html/waw/";

    public static \$PATH_YAML = "/var/www/html/waw/tools/";

    public static \$PATH_YAML_WORKFLOWS = "/var/www/html/waw/workflows/";

    public static \$PATH_YAML_RAW_INPUTS = "/var/www/html/waw/raw_inputs/";

    public static \$PATH_YAML_OUTPUTS = "/var/www/html/waw/output/";

    public static \$PATH_SAG = "/var/www/html/sag/";

    public static \$ACTIVATE_RUN = true;

    public static function dbEncodePass(\$p) {
        return sha1(\$p);
    }

    public static function ldapEncodePass(\$p) {
        return "{SHA}".base64_encode(pack("H*", sha1(\$p)));
    }

}

EOF


# Pull and build container
echo -e "6- Pull and build of the container subwaw-local : \n"

## DOING WITH CAUTION
chmod -R 777 $installdir/mbb-framework
chmod -R 777 $installdir/mbb-download


if [ "${imgsrc}" == "local" ]
then
  ## Building subwaw docker image with dependencies to be able to launch workflows directly from the WebUI.
  ## Please note that it may miss some dependencies (to add in the Dockerfile) !
  docker build -t subwaw-local .
else
  ## using image from dockerhub takes 7mn !
  docker pull mbbteam/subwaw-local:latest
fi

## to push to docker-hub
# docker login --username=yourhubusername --email=youremail@company.com
# docker tag subwaw-local mbbteam/subwaw-local:latest
# docker push mbbteam/subwaw-local:latest

## DOING WITH CAUTION

if [[ "${CREATE_RESULTS}" -eq 1 ]]; then
    chmod -R 777 $resultdir
fi

if [[ "${CREATE_DATA}" -eq 1 ]]; then
    chmod -R 777 $datadir
fi

echo -e "\n\n7- Define mount for container : \n"

is_local_data="$(df ${datadir} -T | tail -1 |awk '{print $2}')"

if [ "${is_local_data}" == "nfs" ]; then
    addr=$(df ${datadir} -T | tail -1 |awk '{split($1,ip,":");print ip[1]}')
    volume=$(readlink ${datadir})
    DOCK_VOL="--mount 'type=volume,dst=/Data,volume-driver=local,volume-opt=type=nfs,\"volume-opt=o=nfsvers=3,addr=${addr}\",volume-opt=device=:${volume}' "
else
    DOCK_VOL="--mount type=bind,src=${datadir},dst=/Data "
fi

is_local_result="$(df ${resultdir} -T | tail -1 |awk '{print $2}')"

if [ "${is_local_result}" == "nfs" ]; then
    addr=$(df ${resultdir} -T | tail -1 |awk '{split($1,ip,":");print ip[1]}')
    volume=$(readlink ${resultdir})
    DOCK_VOL+="--mount 'type=volume,dst=/Results,volume-driver=local,volume-opt=type=nfs,\"volume-opt=o=nfsvers=3,addr=${addr}\",volume-opt=device=:${volume}' "
else
    DOCK_VOL+="--mount type=bind,src=${resultdir},dst=/Results "
fi

DOCK_VOL+="--mount type=bind,src=$installdir/mbb-framework/sag,dst=/var/www/html/sag \
--mount type=bind,src=$installdir/mbb-framework/waw,dst=/var/www/html/waw \
--mount type=bind,src=$installdir/mbb-framework/subwaw,dst=/var/www/html/subwaw \
--mount type=bind,src=$installdir/mbb-framework/waw/tests,dst=/mbb_tests \
--mount type=bind,src=$installdir/mbb-framework/condabin,dst=/var/www/html/condabin \
--mount type=bind,src=$installdir/mbb-download,dst=/Download "



echo $DOCK_VOL


## Determine if a container subwaw-local is running and determine name of the container

echo -e "\n\n8- Determine name of the container : \n"

# cnt=0
# container_name=""
# while [ -z $container_name ]
# do
#   if [ $cnt -eq 0 ]
#   then
#     name="sub-local"
#     empty=$(docker ps -a| grep "subwaw-local" | grep "$name\$")
#     if [ ! -z  "$empty" ]
#     then
#       ((cnt++))
#     else
#       container_name="ok"
#     fi
#   else
#     name="sub-local-"$cnt
#     empty=$(docker ps -a| grep "subwaw-local" | grep "$name\$")
#     if [ ! -z  "$empty" ]
#     then
#       ((cnt++))
#     else
#       container_name="ok"
#     fi
#   fi
# done

name="sub-local-${user%@*}"

empty=$(docker ps -a| grep "subwaw-local" | grep "$name\$")
if [ ! -z  "$empty" ]
then
  echo -e "Problem $name already exist you should stop the container before to run this one.\n"
  exit 1
fi

echo -e "$name \n\n"

## launching the container :

echo -e "9- Launch the container : \n"

if [ "$imgsrc" == "local" ]
then
  cmd="docker run --rm --name $name -d -p $subwaw_port:80 -p $shiny_port:3838 $DOCK_VOL $nbcpus $memory subwaw-local" 
else
  cmd="docker run --rm --name $name -d -p $subwaw_port:80 -p $shiny_port:3838 $DOCK_VOL $nbcpus $memory mbbteam/subwaw-local"
fi

eval $cmd

## if port 80 is used by an other process
# sudo apt-get install net-tools
# sudo netstat -ltnp | grep -w ':80'

echo "#To rerun the container :" > rerun.sh
echo $cmd >> rerun.sh

echo -e $cmd"\n\n"

## adding access control to the subwaw webpage with htpasswd
## There is another htaccess in /var/www/html (see Dockerfile) reading password from /var/www/html/subwaw

## adding the user to the htpasswd file
echo -e "10- Set login and password to access to aplication and add user in container.\n\n"

if [ -n "${password}" ]; then
  docker exec -t  $name htpasswd -cb /var/www/html/subwaw/.htpasswd ${user} ${password}
  echo "docker exec -t  $name htpasswd -cb /var/www/html/subwaw/.htpasswd ${user} ${password}" >> rerun.sh
else
  ## interactive method
  docker exec -t  $name htpasswd -c /var/www/html/subwaw/.htpasswd ${user}
  echo "docker exec -t  $name htpasswd -c /var/www/html/subwaw/.htpasswd ${user}" >> rerun.sh
  ## using pwgen
  #sudo docker exec -t  sub-local htpasswd -bc /var/www/html/subwaw/.htpasswd ${user} $(pwgen -cB 8 1)
fi
## To do/repeat for each new user or to modify original user.

## Create user in container
docker exec -t  $name useradd -d /home/${user} -g root -m ${user} --shell /bin/bash -u ${uid}
echo "docker exec -t  $name useradd -d /home/${user} -g root -m ${user} --shell /bin/bash -u ${uid}" >> rerun.sh


echo -e "User is create with login : ${user} and password : ${password} \n\n"

## install shinyBS because not in dockerhub image for the moment !!
docker exec -t $name Rscript -e 'install.packages("shinyBS", repos="https://cloud.r-project.org")'
echo "docker exec -t $name Rscript -e 'install.packages(\"shinyBS\", repos=\"https://cloud.r-project.org\")'" >> rerun.sh

## reload apache2 server
docker exec -t  $name /etc/init.d/apache2 reload



echo -e "#########################################################################\n"
echo -e "INSTALLATION IS DONE !!\nInstall direcory is in ${installdir}/mbb-framework on local machine and in /var/www/html in container.\nThe container name is ${name}.\nFile donwloaded by mbb workflow run will be store in ${installdir}/mbb-download on local machine and in /Download in container.\nThe data directory on local machine is in $datadir and /Data in container. A sub-directory Databases in $datadir was created to store all your personnal databases (possible by symbolic link). Results directory is in $resultdir on local machine and in /Results in the container.\nThe port use by this container are $subwaw_port to build workflow (subwaw) and $shiny_port to set and execute workflow (sag). You can use : docker ps to display these informations.\nTo go inside the container use the command : docker exec -i -t ${name} /bin/bash\nIf container stop you can rerun it by using bash ${installdir}/mbb-framework/subwaw/rerun.sh\n"


## subwaw WebUI is available at 127.0.0.1/subwaw from the local machine.

## Otherwise execute this command to get your public IP address and to expose subwaw later
wget -qO- https://ipinfo.io/ip | awk -v port=$subwaw_port -v login=$user -v password=$password '{print "You can access the SubWaw workflow generator interface at :  http://"$1":"port" with the login : "login" and the password : "password"\n"}'

## To get a ssh session in your subwaw container
# sudo docker exec -i -t  sub-local /bin/bash
