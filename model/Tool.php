<?php

require_once(__DIR__.'/../dao/DBquery.php');

class Tool {

    var $ID            = 0;
    var $TID           = "";
    var $name          = "";
    var $description   = "";
    var $version       = "";
    var $website       = "";
    var $git           = "";
    var $documentation = "";
    var $article       = "";
    var $multiQC       = "";
    var $install       = "";
    var $yaml          = "";
    var $author        = "";
    var $creationDate  = "";
    var $updateDate    = "";
    var $tags          = "";

    /**
     * Tool constructor.
     */
    public function __construct($ID, $TID, $name, $description, $version, $website, $git, $documentation, $article, $multiQC, $install, $yaml, $author, $creationDate, $updateDate, $tags)
    {
        $this->ID            = $ID;
        $this->TID           = $TID;
        $this->name          = $name;
        $this->description   = $description;
        $this->version       = $version;
        $this->website       = $website;
        $this->git           = $git;
        $this->documentation = $documentation;
        $this->article       = $article;
        $this->multiQC       = $multiQC;
        $this->install       = $install;
        $this->yaml          = $yaml;
        $this->author        = $author;
        $this->creationDate  = $creationDate;
        $this->updateDate    = $updateDate;
        $this->tags          = $tags;
    }

    public function escape($db) {
        $this->TID           = utf8_decode($db->escape($this->TID));
        $this->name          = utf8_decode($db->escape($this->name));
        $this->description   = utf8_decode($db->escape($this->description));
        $this->version       = utf8_decode($db->escape($this->version));
        $this->website       = utf8_decode($db->escape($this->website));
        $this->git           = utf8_decode($db->escape($this->git));
        $this->documentation = utf8_decode($db->escape($this->documentation));
        $this->article       = utf8_decode($db->escape($this->article));
        $this->multiQC       = utf8_decode($db->escape($this->multiQC));
        $this->install       = utf8_decode($db->escape($this->install));
        $this->yaml          = utf8_decode($db->escape($this->yaml));
        $this->tags          = utf8_decode($db->escape($this->tags));
    }

    public function escape2($db) {
        $this->TID           = utf8_encode($db->escape($this->TID));
        $this->name          = utf8_encode($db->escape($this->name));
        $this->description   = utf8_encode($db->escape($this->description));
        $this->version       = utf8_encode($db->escape($this->version));
        $this->website       = utf8_encode($db->escape($this->website));
        $this->git           = utf8_encode($db->escape($this->git));
        $this->documentation = utf8_encode($db->escape($this->documentation));
        $this->article       = utf8_encode($db->escape($this->article));
        $this->multiQC       = utf8_encode($db->escape($this->multiQC));
        $this->install       = utf8_encode($db->escape($this->install));
        $this->yaml          = utf8_encode($db->escape($this->yaml));
        $this->tags          = utf8_encode($db->escape($this->tags));
    }

    public function getInsert() {
        return "INSERT INTO Tool (TID, name, description, version, website, git, documentation, article, multiQC, install, yaml, author, creationDate, updateDate, tags)
        VALUES ('$this->TID', '$this->name', '$this->description', '$this->version', '$this->website', '$this->git', '$this->documentation', '$this->article', '$this->multiQC', '$this->install', '$this->yaml', '$this->author', '$this->creationDate', '$this->updateDate', '$this->tags');";
    }

    public function getUpdate() {
        return "UPDATE Tool
        SET TID='$this->TID', name ='$this->name', description ='$this->description', version ='$this->version', website ='$this->website', git ='$this->git', documentation ='$this->documentation', article ='$this->article', multiQC ='$this->multiQC', install ='$this->install', yaml ='$this->yaml', author ='$this->author', creationDate ='$this->creationDate', updateDate ='$this->updateDate', tags ='$this->tags'
        WHERE ID = '$this->ID';";
    }

    public function getDelete() {
        return "DELETE FROM Tool WHERE ID = '$this->ID';";
    }

}