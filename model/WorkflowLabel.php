<?php

 class WorkflowLabel
 {
    var $ID      = 0;
    var $toolId  = 0;
    var $labelId = 0; 
 
    public function __construct($ID, $toolId, $labelId)
    {
        $this->ID          = $ID;
        $this->toolId = $toolId;
        $this->labelId     = $labelId;
    }

    public function escape($db) {
    }

    public function getInsert() {
        return "INSERT INTO WorkflowLabel (toolId, labelId)
        VALUES ('$this->toolId', '$this->labelId');";
    }

    public function getUpdate() {
        return "UPDATE WorkflowLabel
        SET toolId='$this->toolId', labelId='$this->labelId'
        WHERE ID = '$this->ID';";
    }

    public function getDelete() {
        return "DELETE FROM WorkflowLabel WHERE ID = '$this->ID';";
    }

}