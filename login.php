<?php
    require_once "./inc/php/buildHeader.php";
?>

<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1> Login </h1>
        </div>
        <div class="col-lg-12">
            <form action="./action/action_login.php" method="post">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="texte" class="form-control" id="username" name="username" placeholder="username">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="password">
                </div>

                <button type="submit" class="btn btn-primary">Sign in</button>
            </form>
            <br/><br/><br/><br/>
        </div>
    </div>
</div>

<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
