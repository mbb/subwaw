<?php

require_once(__DIR__.'/model/GTool.php');
require_once(__DIR__.'/model/GPort.php');
require_once(__DIR__.'/model/Workflow.php');
require_once(__DIR__.'/model/Tool.php');

/**
 * 
 * Generate the raw inputs yaml value
 * 
 * @return string
 */
function generateYAMLRAWINPUTS() {

    $inputs = scandir(Conf::$PATH_YAML_RAW_INPUTS);

    $results = [];

    foreach ($inputs as $input) {

        if($input != "." && $input != "..") {

            $value = explode(".", $input);

            if(end($value) == "yaml") {
                array_push($results, reset($value));
            }

        }

    }

    $MBB_TOOLS_RAW_INPUT = "var MBB_TOOLS_RAW_INPUT = [";

    $index = 0;
    foreach ($results as $name) {
        if($index == 0) {
            $MBB_TOOLS_RAW_INPUT .= "'".$name."'";
        }
        else {
            $MBB_TOOLS_RAW_INPUT .= ", '".$name."'";
        }
        $index++;
    }

    $MBB_TOOLS_RAW_INPUT .= "];";

    return $MBB_TOOLS_RAW_INPUT;

}

function getJSONTools($tid = "") {
    $results = [];

    $dircategories = scandir(Conf::$PATH_YAML);
    
    foreach ($dircategories as $dircategory)
    {
        if (is_dir ($path_waw_tools . $dircategory ) && $dircategory != "." && $dircategory != ".." )
        {
            $tools   = scandir(Conf::$PATH_YAML.$dircategory);
            foreach ($tools as $tool) 
            {
                $path_yaml = Conf::$PATH_YAML  .$dircategory . "/" . $tool . "/" . $tool . ".yaml";
                if(file_exists($path_yaml)) 
                {
                    $data = yaml_parse_file($path_yaml);
                    if($data == false) {
                        continue;
                    }

                    $data["yaml"] = file_get_contents($path_yaml);
                    if($tid == "") {
                        array_push($results, $data);
                    }
                    else if($tool == $tid) {
                        array_push($results, $data);
                        break;
                    }
                }
            }
        }
    }
    return $results;
}

/**
 * Get the tools from YAML file
 * 
 * @return Array of Tool
 */
function getYAMLTools() {
    $path_waw_tools = Conf::$PATH_YAML;
    $results = [];
    $dircategories = scandir(Conf::$PATH_YAML);
    foreach ($dircategories as $dircategory)
    {
        if (is_dir ($path_waw_tools . $dircategory ) && $dircategory != "." && $dircategory != ".." )
        {
            $tools   = scandir(Conf::$PATH_YAML.$dircategory);
            foreach ($tools as $tool) 
            {
                if($tool != "." && $tool != ".." && $tool != "null" && $tool != "global.yaml") 
                {
                    $path_yaml = Conf::$PATH_YAML .$dircategory. "/".$tool. "/". $tool . ".yaml";
                    if(file_exists($path_yaml)) 
                    {
                    $data = yaml_parse_file($path_yaml);
                        if($data == false) {
                            continue;
                        }
                                        
                        $ID  = 0;
                        $TID = $tool;
                        $name = $data["name"];
                        $description = $data["description"];
                        $version = $data["version"];
                        $website = $data["website"];
                        $git = $data["git"];
                        $documentation = $data["documentation"];
                        $article = $data["article"];
                        $multiQC = $data["multiqc"];
                        $install = "";
                        $yaml = "";
                        $author = "mmassaviol";
                        $creationDate = "";
                        $updateDate = "";
                        $tags = [];

                        if(array_key_exists("commands", $data)) {

                            foreach ($data["commands"] as $command) {

                                #if($command["category"] != "indexing") {
                                    array_push($tags, $command["category"]);
                                #}

                            }
                        }

                        $tags = implode(",", array_unique($tags));
                        $tool = new Tool($ID, $TID, $name, $description, $version, $website, $git, $documentation, $article, $multiQC, $install, $yaml, $author, $creationDate, $updateDate, $tags);
                        array_push($results, $tool);
                    }
                }
            }
      }
    }
    return $results;
}

function getJSONWorkflow($wname) {


    $path_yaml = Conf::$PATH_YAML_WORKFLOWS . $wname . "/" . $wname . ".yaml";
    $path_json = Conf::$PATH_YAML_WORKFLOWS . $wname . "/" . $wname . ".json";

    $wk = NULL;

    if(file_exists($path_yaml)) {

        $data = yaml_parse_file($path_yaml);

        if($data == false) {
            return NULL;
        }
        $name         = $data["name"];
        $docker_name  = $data["docker_name"];


        if($name != "Variant_calling_benchmark" && $name != "Variant_calling_benchmark_From_bam") {
            $git = "https://gitlab.mbb.univ-montp2.fr/khalid/" . $docker_name;
        }

        if(file_exists($path_json)) {
            $graphicYAML = $path_json;
        }             

        $wk = new Workflow($ID, $name, $docker_name, $description, $version, $author, $creationDate, $updateDate, $tags, $graphicYAML, $yaml, $git);
    }

    return $wk;
}

/**
 * 
 * Get workflow from yaml with is name
 * 
 * @return Workflow | NULL
 */
function getYAMLWorkflow($wname) {


    $path_yaml = Conf::$PATH_YAML_WORKFLOWS . $wname . "/" . $wname . ".yaml";
    $path_json = Conf::$PATH_YAML_WORKFLOWS . $wname . "/" . $wname . ".json";

    $wk = NULL;

    if(file_exists($path_yaml)) {

        $data = yaml_parse_file($path_yaml);

        if($data == false) {
            return NULL;
        }

        $ID           = 0;
        $name         = $data["name"];
        $docker_name  = $data["docker_name"];
        $description  = $data["description"];
        $version      = $data["version"];
        $author       = $data["author"];
        $creationDate = "";
        $updateDate   = "";
        $tags         = [];
        $graphicYAML  = "";
        $yaml         = "";
        $git          = "";

        if($name != "Variant_calling_benchmark" && $name != "Variant_calling_benchmark_From_bam") {
            $git = "https://gitlab.mbb.univ-montp2.fr/khalid/" . $docker_name;
        }

        if(file_exists($path_json)) {
            $graphicYAML = $path_json;
        }             

        $wk = new Workflow($ID, $name, $docker_name, $description, $version, $author, $creationDate, $updateDate, $tags, $graphicYAML, $yaml, $git);
    }

    return $wk;
}

function getJSONWorkflows($tid = "") {

    $results = [];

    $workflows = scandir(Conf::$PATH_YAML_WORKFLOWS);

    foreach ($workflows as $workflow) {

        if($workflow != "." && $workflow != ".." && $workflow != "global_functions.py" && $workflow != "global_rules.snakefile") {

            $path_yaml = Conf::$PATH_YAML_WORKFLOWS . $workflow . "/" . $workflow . ".yaml";
            $path_json = Conf::$PATH_YAML_WORKFLOWS . $workflow . "/" . $workflow . ".json";

            if(file_exists($path_yaml)) {

                $data = yaml_parse_file($path_yaml);

                if($data == false) {
                    continue;
                }

                $ID           = 0;
                $name         = $data["name"];
                $docker_name  = $data["docker_name"];
  
                $git          = "";

                if($name != "Variant_calling_benchmark" && $name != "Variant_calling_benchmark_From_bam") {
                    $git = "https://gitlab.mbb.univ-montp2.fr/khalid/" . $docker_name;
                }

                $data["git"] = $git;

                $json = "";

                if(file_exists($path_json)) {
                    $json = json_decode(file_get_contents($path_json));
                }
                
                $data["json"] = $json;
                $data["yaml"] = file_get_contents($path_yaml);

                if($tid == "") {
                    array_push($results, $data);
                }
                else if($workflow == $tid) {
                    array_push($results, $data);
                    break;
                }

            }
        }
    }

    return $results;
}

/**
 * Get the workflows from YAML file
 * 
 * @return Array of Workflow
 */
function getYAMLWorkflows() {

    $results = [];

    $workflows = scandir(Conf::$PATH_YAML_WORKFLOWS);

    foreach ($workflows as $workflow) 
    {

        if($workflow != "." && $workflow != ".." && $workflow != "global_functions.py" && $workflow != "global_rules.snakefile") {

            $path_yaml = Conf::$PATH_YAML_WORKFLOWS . $workflow . "/" . $workflow . ".yaml";
            $path_json = Conf::$PATH_YAML_WORKFLOWS . $workflow . "/" . $workflow . ".json";

            if(file_exists($path_yaml)) {

                $data = yaml_parse_file($path_yaml);

                if($data == false) {
                    continue;
                }

                $ID           = 0;
                $name         = $data["name"];
                $docker_name  = $data["docker_name"];
                $description  = $data["description"];
                $version      = $data["version"];
                $author       = $data["author"];
                $creationDate = "";
                $updateDate   = "";
                $tags         = "";
                $graphicYAML  = "";
                $yaml         = "";
                $git          = "";

                if($name != "Variant_calling_benchmark" && $name != "Variant_calling_benchmark_From_bam") {
                    $git = "https://gitlab.mbb.univ-montp2.fr/khalid/" . $docker_name;
                }

                if(file_exists($path_json)) {
                    $graphicYAML = $path_json;
                }             

                $wk = new Workflow($ID, $name, $docker_name, $description, $version, $author, $creationDate, $updateDate, $tags, $graphicYAML, $yaml, $git);
                array_push($results, $wk);

            }
        }
    }

    return $results;
}

/**
 * 
 * @return Array
 */
function generateDATAYAML() {

    $path_waw_tools = Conf::$PATH_YAML;

    $path_global = $path_waw_tools."global.yaml";

    $data = [];

    $gdata = [];

    $title = [];

    $number = [];

    if(file_exists($path_global)) {
        $global = yaml_parse_file($path_global);

        $categories = $global["categories2"];

        foreach ($categories as $c) {
            $data[$c["name"]] = [];
            $gdata[$c["name"]] = [];
            $title[$c["name"]] = $c["title"];
            $number[$c["name"]] = 0;
        }

        $dt = $global["data"];

        foreach ($dt as $d) {
            array_push($data[$d["category"]], $d["name"]);
        }
    }

    foreach ($data as $key => $value) {
        $gtool = new GTool("blank", "", $key, "", "");
        $gtool->initBlankSize();
        array_push($gdata[$key], $gtool);
        $gtool = new GTool("blank", "", $key, "", "");
        $gtool->initBlankSize();
        array_push($gdata[$key], $gtool);

        foreach($value as $v) {

            $text = $v;

            if($text == "reads") {
                $text = "read";
            }

            $gtool = new GTool($v, $text, $key, "", "");
            $gtool->category = "Data";
            
            if($key == "database") {
                $gtool->fill = "#16a085";
            }
            else {
                $gtool->fill = "#2ecc71";
            }

            $gport = new GPort("o1", $v, ("raw_".$v), "", "OUT");
            $gtool->addOutputPort($gport);
            array_push($gdata[$key], $gtool);
            $number[$key]++;
        }

    }

    $MBB_TOOLS = "var MBB_DATA = [];";
    foreach ($gdata as $keyC => $tools) {


        $MBB_TOOLS .= "MBB_DATA['".$keyC."'] = [";
        $index = 0;
        foreach ($tools as $tool) {
    
            if($index == 0) {
                $MBB_TOOLS .= $tool->generate();
            }
            else {
                $MBB_TOOLS .= ",".$tool->generate();
            }
            $index++;
    
        }
        $MBB_TOOLS .= "];"; 
    }
    
    return [$MBB_TOOLS, $title, $number];
}


/**
 * 
 * @return Array
 */
function generateTOOLSYAML() {

$path_waw_tools = Conf::$PATH_YAML;

$path_global = $path_waw_tools."global.yaml";


$categories = [];
$categories_title = [];
$categories_number = [];

//$categories['all'] = [];
//$categories_title['all'] = 'All';
//$gtool = new GTool("blank", "", "all", "", "");
//$gtool->initBlankSize();
//array_push($categories["all"], $gtool);
//$gtool = new GTool("multiqc", "Multiqc", "all", "Multiqc", "1");
//$gtool->fill = "#e67e22";
//$gtool->category = "Repport";
//$gport = new GPort("i1", "*", "data", "", "IN");
//$gtool->addInputPort($gport);
//$gport = new GPort("o2", "html", "rapport", "Rapport", "OUT");
//gtool->addOutputPort($gport);
//$gtool->initSize();
//array_push($categories["all"], $gtool);


if(file_exists($path_global)) {

    $global = yaml_parse_file($path_global);

    if(array_key_exists("categories", $global)) {

        $ycategories = $global["categories"];

        foreach ($ycategories as $yc) {
            $categories[$yc["name"]] = [];
            $categories_title[$yc["name"]] = $yc["title"];
            $categories_number[$yc["name"]] = 0;
            $gtool = new GTool("blank", "", $yc["name"], "", "");
            $gtool->initBlankSize();
            array_push($categories[$yc["name"]], $gtool);
        }    
    }

}
else {
    $categories['other'] = [];
    $categories_title['other'] = 'Other';
    $categories_number['other'] = 0;
    $gtool = new GTool("blank", "", $yc["name"], "", "");
    $gtool->initBlankSize();
    array_push($categories[$yc["name"]], $gtool);
}


$tot = 0;
$val = 0;
#this is a category directory
$dircategories = scandir($path_waw_tools);

foreach ($dircategories as $dircategory) 
{
  if (is_dir ($path_waw_tools . $dircategory ) && $dircategory != "." && $dircategory != ".." )
  {
    $tools = scandir($path_waw_tools.$dircategory);
    foreach ($tools as $tool) 
    {
      if($tool != "." && $tool != ".." && $tool != "null") 
      {
        $path_yaml = $path_waw_tools . $dircategory ."/". $tool . "/" . $tool . ".yaml";
        if(file_exists($path_yaml)) 
        {
            $data = yaml_parse_file($path_yaml);
          
            if($data == false) {continue; }
            
            if(array_key_exists("commands", $data)) {
                foreach ($data["commands"] as $command) {
                    $tot++;
                    if(array_key_exists("category", $command)) {
                        $val++;
                        $name = "";
                        if(sizeof($data["commands"]) == 1) {
                            if(array_key_exists("name", $command)) {
                                $name = $command["name"];
                            }
                            else {
                                $name = $data["id"];
                            }
                        }
                        else {
                            $name = $command["name"];
                        }

                        $text = $data["name"];

                        if(array_key_exists("cname", $command)) {
                            $text = $command["cname"];
                        }
                        $category = $command["category"];

                        #if($category == "indexing") {
                        #    continue;
                        #}
                        $description = $data["description"];
                        $version = $data["version"];
                        $gtool = new GTool($name, $text, $category, $description, $version);
                        $inputs = $command["inputs"];

                        $ni = 0;
                        foreach ($inputs as $input) {
                            $ni++;
                            $pname = "i".$ni;
                            $ptype = "";

                            if(array_key_exists("type", $input)) {
                                $ptype = $input["type"];
                            }

                            $pvalue = "";
                            if(array_key_exists("name", $input)) {
                                $pvalue = $input["name"];
                            }

                            $pdescription = "";
                            if(array_key_exists("description", $input)) {
                                $pdescription = $input["description"];
                            }

                            $from = "";
                            if(array_key_exists("from", $input)) {
                                $from = $input["from"];
                            }

                            $list = false;
                            if(array_key_exists("list", $input)) {
                                $list = $input["list"];
                            }

                            if($ptype != "") {
                                $gport = new GPort($pname, $ptype, $pvalue, $pdescription, "IN", $from, $list);
                                $gtool->addInputPort($gport);
                            }
                        }

                        if($ni == 0) {
                            //var_dump($gtool);
                            //die();
                        }

                        $outputs = $command["outputs"];

                        $no = 0;
                        foreach ($outputs as $output) {
                            $no++;

                            $pname = "o".$no;

                            $ptype = "";
                            if(array_key_exists("type", $output)) {
                                $ptype = $output["type"];
                            }

                            $pvalue = "";
                            if(array_key_exists("name", $output)) {
                                $pvalue = $output["name"];
                            }

                            $pdescription = "";
                            if(array_key_exists("description", $output)) {
                                $pdescription = $output["description"];
                            }

                            $from = "";
                            if(array_key_exists("from", $output)) {
                                $from = $output["from"];
                            }

                            $list = false;
                            if(array_key_exists("list", $output)) {
                                $list = $input["list"];
                            }
                             
                            if($ptype != "") {
                                
                                if($name == "dada2" && $ptype == "png") {
                                    continue;
                                }
                                else {
                                    $gport = new GPort($pname, $ptype, $pvalue, $pdescription, "OUT", $from, $list);
                                    $gtool->addOutputPort($gport);
                                }
                            }
                        }
                        $gtool->initSize();
                        #echo $path_yaml . ": ". $category . "  !<br>";  
                        array_push($categories[$category], $gtool);
                        $categories_number[$category] += 1;
                    }
                }
            }
            #echo $path_yaml . " done !<br>";    
        }
    }
 }
}
}

$MBB_TOOLS = "var MBB_TOOLS = [];";

$MBB_STEP_CATEGORY = "var MBB_STEP_CATEGORY = [];";

foreach ($categories as $keyC => $tools) {

    $MBB_STEP_CATEGORY .= "MBB_STEP_CATEGORY['".$keyC."'] = [";


    $MBB_TOOLS .= "MBB_TOOLS['".$keyC."'] = [";
    $index = 0;
    foreach ($tools as $tool) {

        if($index == 0) {
            $MBB_TOOLS .= $tool->generate();
            $MBB_STEP_CATEGORY .= "'".$tool->name."'";
        }
        else {
            $MBB_TOOLS .= ",".$tool->generate();
            $MBB_STEP_CATEGORY .= ", '".$tool->name."'";
        }
        $index++;
        //$MBB_TOOLS .= "<br/>";

    }
    $MBB_TOOLS .= "];"; 
    //$MBB_TOOLS .= "<br/><br/>";
    $MBB_STEP_CATEGORY .= "];"; 
}

return [$MBB_TOOLS." ".$MBB_STEP_CATEGORY, $categories_number, $categories_title];

}

/**
 * 
 * Create HTML palette item
 * 
 * @param {Array} titleArray
 * @param {Array} numberArray
 * @param {String} templateID 
 * @param {int} width
 * @param {int} height
 */
function createPalette($titleArray, $numberArray, $templateID, $width, $height, $node_type) {

    if($node_type) {
        echo '<h4>Search &nbsp;<span id="'.$templateID.'_'.'searchSpan" class="badge badge-pill badge-primary">0</span> </h4>';

    }
    else {
        echo '<h4>Search &nbsp;<span id="'.$templateID.'_'.'searchSpan" class="badge badge-pill badge-success">0</span> </h4>';
    }


    echo '<div>';
        echo '<input id="'.$templateID.'_'."searchInput_input".'" type="text" class="form-control" onkeyup="searchNodes(\''.$templateID.'\', '.$node_type.')" >';
        echo '  <div id="'.$templateID.'_'."searchInput".'" style="width: '.$width.'px; height: '.$height.'px"></div>';
    echo '</div>';
    foreach ($titleArray as $keyT => $title) {
        if($title != "All") {
            if ($node_type) {
                echo '<h4>'.$title.'&nbsp;<span class="badge badge-pill badge-primary">'.$numberArray[$keyT].'</span></h4>';
            }
            else {
                echo '<h4>'.$title.'&nbsp;<span class="badge badge-pill badge-success">'.$numberArray[$keyT].'</span></h4>';
            }

            echo '<div>';
            echo '  <div id="'.$templateID.'_'.$keyT.'" style="width: '.$width.'px; height: '.$height.'px"></div>';
            echo '</div>';
        }
    }
}

function drawImportModal($id, $title) {
    echo '

    <div id="'.$id.'" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
    <form id = "formImportJSON" action="workflow.php" method="post" enctype="multipart/form-data">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">'.$title.': </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <input type="file" accept="application/JSON" name="fileToUpload" id="fileToUpload" onchange="handleJsonFiles(this)">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default btn-md" data-dismiss="modal">Close</button>
            <!--<input type="submit" class="btn btn-success btn-md" value="Import" name="submit">-->
        </div>
        </div>
    </form>
    </div>
    </div>
    ';
}

function drawPreviewModal($id, $title) {
    echo '

    <div id="'.$id.'" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">'.$title.': </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <textarea id="workflowPreview"  name="preview" class="form-control" rows="20"></textarea>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default btn-md" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>
    ';
}

function drawRunSuccessModal($id, $title) {
    echo '

    <div id="'.$id.'" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">'.$title.': </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div id="workflowRunning"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default btn-md" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>
    ';
}

function drawRunModal($id, $title, $name) {
    echo '

    <div id="'.$id.'" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
        <form id="formWorkflowRun" action="./action/action_workflow.php" method="post">

        <div class="modal-header">
            <h4 class="modal-title">'.$title.': </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">

            <input id="actionWorkflow" type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="run">
            <input id="pngWorkflowRun" type="text" class="form-control" style="display: none;" placeholder="png" name="png" value="">
            <textarea id="previewWorkflowRun" class="form-control" style="display: none;" placeholder="preview" name="preview" value=""></textarea>
            <input id="jsonWorkflowRun" type="text" class="form-control" style="display: none;" placeholder="preview" name="json" value="">

            <label for="Warning" class="">This will kill previously the running workflow !!! <span style="font-size:20px;" class="status text-danger">*</span>:</label>


            <label for="nameWRun" class="">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
            <input type="text" class="form-control" id="nameWRun" name = "name" value = "'.$name.'" required>
            <small class="form-text text-muted">No space in Name value.</small>

            <label for="descriptionWRun">Description:</label>
            <textarea class="form-control" rows="3" id="descriptionWRun" maxlength="500" name ="description">My workflow</textarea>
    
            <label for="versionWRun">Version:</label>
            <input type="text" class="form-control" id="versionWRun" name="version" value="0.0.1" required>


            <label for="authorWRun">Author:</label>
            <input type="text" class="form-control" id="authorWRun" name="type" value="MBB">

            <label for="userWRun">user login for Shiny authentication :</label>
            <input type="text" class="form-control" id="userWRun" name="user" value="" >

            <label for="passWRun">user password for Shiny authentication :</label>
            <input type="text" class="form-control" id="passWRun" name="passwd" value="" >            

            <div id="runSpinner" class="loaderMBB text-center">
               <img src="/subwaw/data/spinner-icon-gif-10.gif" width="350" />
            </div>


        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default btn-md" data-dismiss="modal">Close</button>
            <input id="runButton" class="btn btn-success btn-md" value="Run" name="run" onclick="runWorkflow();">
        </div>
    
        </form>
        </div>
    </div>
    </div>
    ';
}

function drawDownloadModal($id, $title) {
    echo '

    <div id="'.$id.'" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
        <form id="formWorkflow" action="./action/action_workflow.php" method="post">

        <div class="modal-header">
            <h4 class="modal-title">'.$title.': </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">

            <input id="actionWorkflow" type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="download">
            <input id="pngWorkflow" type="text" class="form-control" style="display: none;" placeholder="png" name="png" value="">
            <textarea id="previewWorkflow" class="form-control" style="display: none;" placeholder="preview" name="preview" value=""></textarea>
            <input id="jsonWorkflow" type="text" class="form-control" style="display: none;" placeholder="preview" name="json" value="">


            <label for="nameW" class="">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
            <input type="text" class="form-control" id="nameW" name = "name" value = "" required>
            <small class="form-text text-muted">No space in Name value.</small>

            <label for="descriptionW">Description:</label>
            <textarea class="form-control" rows="3" id="descriptionW" maxlength="500" name ="description"></textarea>
    
            <label for="versionW">Version:</label>
            <input type="text" class="form-control" id="versionW" name="version" value="0.0.1" required>


            <label for="authorW">Author:</label>
            <input type="text" class="form-control" id="authorW" name="type" value="MBB">

            <label for="userW">user login to enable Shiny app authentication:</label>
            <input type="text" class="form-control" id="userW" name="user" value="" >

            <label for="passW">user password for Shiny authentication :</label>
            <input type="text" class="form-control" id="passW" name="passwd" value="" >            

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default btn-md" data-dismiss="modal">Close</button>
            <input class="btn btn-success btn-md" value="Download" name="download" onclick="downloadWorkflow();">
        </div>
    
        </form>
        </div>
    </div>
    </div>
    ';
}

/**
 * Get th default valid json for empty workflow
 */
function getDefaultJSON() {
    return '{ "class": "go.GraphLinksModel", "linkFromPortIdProperty": "fromPort", "linkToPortIdProperty": "toPort", "nodeDataArray": [], "linkDataArray": []}'; 
}

/**
 * Print Javascript Alert
 */
function printJavascriptAlert($message) {
    echo '<script>alert("'.$message.'");</script>';
}

?>
