<?php

session_start ();

define("PAGE","CREATE_WORKFLOW");

require_once("./inc/php/buildHeader.php");

require_once("./dao/DBquery.php");

require_once("./tool.php");

$db = new DBqueryLite();

$workflow = NULL;

$import = false;

if(isset($_GET['workflowid']) && isset($_GET['action'])) {
  $workflow = getYAMLWorkflow($_GET['workflowid']);

  if($_GET['action'] == "import" ) {
    $import = true;
  }
}

$defaultJSON = getDefaultJSON();
$loadJSON = $defaultJSON;

if(isset($_FILES['fileToUpload']) ) {
  $json = $_FILES["fileToUpload"];
  $type = $json['type'];
  if($type == "application/json") {
    $loadJSON = file_get_contents($json['tmp_name']);
    $objjson = json_decode($loadJSON, true);
    if(array_key_exists('version',$objjson)) {
      if($objjson['version'] < Conf::$VERSION) {
        $loadJSON = "";
        printJavascriptAlert("Error file version: Your version of subwaw json is too old for import.");
      }
    }
    else {
      $loadJSON = "";
      printJavascriptAlert("Error file version: Your version of subwaw json is too old for import.");
    }
  }
  else {
    printJavascriptAlert("Error file format: Use JSON file type.");
  }
}

$MBB_TOOLS_RAW_INPUT = generateYAMLRAWINPUTS();


$TOOL_INFO = generateTOOLSYAML();

$TOOLS_JAVASCRIT = $TOOL_INFO[0];
$TOOLS_NUMBER = $TOOL_INFO[1];
$TOOLS_TITLE = $TOOL_INFO[2];

$DATA_INFO = generateDATAYAML();
$DATA_JAVASCRIPT = $DATA_INFO[0];
$DATA_TITLE = $DATA_INFO[1];
$DATA_NUMBER = $DATA_INFO[2];

?>
<form id="myForm" action="runworkflow.php?action=preview&workflowid=<?php echo $_GET['workflowid']; ?> " method="post">
<?php
		echo '<input type="hidden" class="form-control" id="yamlText" name = "yamlText" value = "TTTT" required>';
		echo '<input id="helperpng" type="hidden" class="form-control" style="display: none;" placeholder="png" name="helperpng" value="">';
    echo '<input type="hidden" class="form-control" id="jsonWorkFlow" name = "jsonWorkFlow" value = "JSON" required>';
?>
</form>


<div class="container-fluid">
    <div class="row">
    <div id="sample">
    <!-- <div style="width: 100%; display: flex; justify-content: space-between">-->
    <div style="width: 100%; padding-left: 5px; margin: 5px">
      
      <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#importJSON" ><i class="fas fa-file-import"></i>&nbsp;Import JSON</button>
      <a  class="btn btn-primary btn-sm" style= "color: white !important;" onclick="exportJSON(this)" download="workflow.json"><i class="fas fa-file-export"></i>&nbsp;Export JSON</a>
      <a  class="btn btn-primary btn-sm" style= "color: white !important;" onclick="exportPng(this)" download="workflow.png"><i class="fas fa-file-export"></i>&nbsp;Export PNG</a>
      <!--<button type="button" id="previewButton" class="btn btn-dark btn-sm"  data-toggle="modal" data-target="#prefiewYAML" onclick="generateWorkflow(true, false)"><i class="fas fa-eye"></i>&nbsp;Preview config file</button> -->

	  <button type="button" id="previewButton" class="btn btn-primary btn-sm"  onclick="editWorkflow()"><i class="fas fa-cogs"></i>&nbsp;Run WF</button>
	  

      <!-- <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#downloadProject" ><i class="fas fa-file-archive"></i>&nbsp;Download</button> -->
      
      <?php 

      if(Conf::$ACTIVATE_RUN) {
        //echo '<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#runProject" onclick="hideSpinner();" ><i class="fas fa-play"></i>&nbsp;Run</button>';
      }
      ?>

      <button id="CleanButton" class="btn btn-danger btn-sm" onclick="clean()"><i class="fas fa-trash"></i>&nbsp;Clean workflow</button>
      <!-- <div id="myOverviewDiv" style="width: 300px; height: 100px; margin-bottom: 2px; border: solid 1px black"></div> -->
    </div>
  <div id="toolsDiv" style="width: 100%; display: flex; justify-content: space-between">
    <div id="infoBoxHolder"></div>
    <div style="width: 300px; margin-right: 2px; background-color: whitesmoke; border: solid 1px black"> 

      <div id="accordion">
        <?php createPalette($TOOLS_TITLE, $TOOLS_NUMBER, "myPaletteDiv", 300, 400, true); ?>
      </div>
    </div>
    <div id="myDiagramDivParent" style="flex-grow: 1; height: 1000px; width: 1100px;">
      <div id="myDiagramDiv" style="flex-grow: 1; height: 1000px; width: 1100px; border: solid 1px black"></div>
      <div id="consoleRow" style="flex-grow: 1; margin-top: 2px; width: 1337px; border: solid 1px black;">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="console-tab" data-toggle="tab" href="#console" role="tab" aria-controls="console" aria-selected="true">Console</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="console" role="tabpanel" aria-labelledby="console-tab"><div id="consolePreview"  class="form-control" style="overflow:scroll;"></div></div>
        </div>
      </div>
    </div>
    <div style="width: 200px; margin-left: 2px; background-color: whitesmoke; border: solid 1px black">
      <!-- <div id="myPaletteDiv_all" style="width: 200px; height: 150px"></div> -->
      <div id="accordion2">
        <?php createPalette($DATA_TITLE, $DATA_NUMBER, "myPaletteDivType", 195, 400, false); ?>
      </div>
    </div>
  </div>
  </div>
  </div>
  
  <br/>

  <?php 

drawImportModal("importJSON", "Import from JSON");

drawPreviewModal("prefiewYAML", "Preview config file");

drawDownloadModal("downloadProject", "Download workflow");

if(Conf::$ACTIVATE_RUN) {
  drawRunModal("runProject", "Run workflow", "workflow_".rand(10000,99999));
  drawRunSuccessModal("runSuccessModal", "Running workflow");
}

?>
</div>

<script src="./inc/js/node_modules/gojs/assets/js/jquery.min.js"></script>
<script src="./inc/js/node_modules/gojs/assets/js/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script src="./inc/js/node_modules/gojs/release/go.js"  ></script>
<script src="./inc/js/Figures.js"  ></script>
<script src="./inc/js/workflow.js"></script>
<script src="./inc/js/jtool.js"></script>
<script src="./inc/js/jport.js"></script>
<script src="./inc/js/jtoolinfobox.js"></script>

<script>

<?php echo $TOOLS_JAVASCRIT; ?>

<?php echo $MBB_TOOLS_RAW_INPUT; ?>

<?php echo $DATA_JAVASCRIPT; ?>

var arrStep = {};

window.onresize = reportWindowSize;

var createPalette;
var resetPalette;

function init() {

     //document.getElementById('myOverviewDiv').style.width= (window.screen.availWidth-12)+'px'; 

      var clickedTool = true;

      if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
      var $ = go.GraphObject.make;  // for conciseness in defining templates

      myDiagram =
        $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
          {
            grid: $(go.Panel, "Grid",
              $(go.Shape, "LineH", { stroke: "lightgray", strokeWidth: 0.5 }),
              $(go.Shape, "LineH", { stroke: "gray", strokeWidth: 0.5, interval: 10 }),
              $(go.Shape, "LineV", { stroke: "lightgray", strokeWidth: 0.5 }),
              $(go.Shape, "LineV", { stroke: "gray", strokeWidth: 0.5, interval: 10 })
            ),
            "draggingTool.dragsLink": true,
            "draggingTool.isGridSnapEnabled": true,
            "linkingTool.isUnconnectedLinkValid": true,
            "linkingTool.portGravity": 20,
            "relinkingTool.isUnconnectedLinkValid": true,
            "relinkingTool.portGravity": 20,
            "relinkingTool.fromHandleArchetype":
              $(go.Shape, "Diamond", { segmentIndex: 0, cursor: "pointer", desiredSize: new go.Size(8, 8), fill: "tomato", stroke: "darkred" }),
            "relinkingTool.toHandleArchetype":
              $(go.Shape, "Diamond", { segmentIndex: -1, cursor: "pointer", desiredSize: new go.Size(8, 8), fill: "darkred", stroke: "tomato" }),
            "linkReshapingTool.handleArchetype":
              $(go.Shape, "Diamond", { desiredSize: new go.Size(7, 7), fill: "lightblue", stroke: "deepskyblue" }),
            "rotatingTool.handleAngle": 270,
            "rotatingTool.handleDistance": 30,
            "rotatingTool.snapAngleMultiple": 15,
            "rotatingTool.snapAngleEpsilon": 15,
            "undoManager.isEnabled": true
          });

      // when the document is modified, add a "*" to the title and enable the "Save" button
      myDiagram.addDiagramListener("Modified", function(e) {
        var button = document.getElementById("SaveButton");
        if (button) button.disabled = !myDiagram.isModified;
        var idx = document.title.indexOf("*");
        if (myDiagram.isModified) {
          if (idx < 0) document.title += "*";
        } else {
          if (idx >= 0) document.title = document.title.substr(0, idx);
        }
      });

      /**
       * Template for Port
       */
      function makeItemTemplate(leftside) {
        return $(go.Panel, "Auto",
          { margin: new go.Margin(3, 0) },  // some space between ports
          $(go.Shape,
            {
              name: "SHAPE",
              fill: "lightgray", strokeWidth: 2,
              geometryString: "F1 m 0,0 l 5,0 1,4 -1,4 -5,0 1,-4 -1,-4 z",
              spot1: new go.Spot(0, 0, 5, 1),
              spot2: new go.Spot(1, 1, -5, 0),
              toSpot: go.Spot.Left,
              toLinkable: leftside,
              fromSpot: go.Spot.Right,
              fromLinkable: !leftside,
              cursor: "pointer"
            },
            new go.Binding("stroke"),
            new go.Binding("portId", "value"),
            new go.Binding("type"),
            new go.Binding("ptype"),
            new go.Binding("list")),
            $(go.TextBlock,
              new go.Binding("text", "name")
            )
        );
      }

      var nodeSelectionAdornmentTemplate =
        $(go.Adornment, "Auto",
          $(go.Shape, { fill: null, stroke: "deepskyblue", strokeWidth: 1.5, strokeDashArray: [4, 2] }),
          $(go.Placeholder)
        );

      /**
       * 
       * Template for "Tool" element
       * 
       */
      myDiagram.nodeTemplate =
        $(go.Node, "Spot",
          { locationSpot: go.Spot.Center },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          { selectable: true, selectionAdornmentTemplate: nodeSelectionAdornmentTemplate, textEditable: false },
          new go.Binding("angle").makeTwoWay(),
          {
            click: function(e, obj) { if(clickedTool) {clickedTool = false; hideToolTip(); }else {clickedTool = true; showToolTip(e, obj);}},
            mouseOver: function(e, obj) { if(clickedTool) { showToolTip(e, obj);}},   
            mouseLeave: function(e, obj) {  hideToolTip(); }
          },
          // the main object is a Panel that surrounds a TextBlock with a Shape
          $(go.Panel, "Auto",
            { name: "PANEL" },
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            $(go.Shape, "FramedRectangle", 
              new go.Binding("desiredSize", "size2", go.Size.parse),
              new go.Binding("figure"),
              new go.Binding("fill"),
              new go.Binding("strokeWidth"),
              
              ),

              $(go.Shape, "Rectangle",
            { fill: "white" },
            new go.Binding("desiredSize", "size2", function(n) {

              var shape = go.Size.parse(n);

              if(shape.width == 1) {
                return go.Size.parse("0 0");
              }
              else {
                return go.Size.parse((shape.width-16)+" "+(shape.height-16));
              }
            }),
            new go.Binding("figure"),
            new go.Binding("version"),
            new go.Binding("strokeWidth")),
            $(go.TextBlock,
              {
                font: "bold 11pt Helvetica, Arial, sans-serif",
                margin: 8,
                maxSize: new go.Size(160, NaN),
                wrap: go.TextBlock.WrapFit,
                editable: true
              },
              new go.Binding("text").makeTwoWay())


          ),
          $(go.Panel, "Vertical",
            { name: "LEFTPORTS", alignment: new go.Spot(0, 0.45, 0.5, 7) },
            new go.Binding("itemArray", "inservices"),
            { itemTemplate: makeItemTemplate(true) }
          ),
          $(go.Panel, "Vertical",
            { name: "RIGHTPORTS", alignment: new go.Spot(1, 0.45, 0.5, 7) },
            new go.Binding("itemArray", "outservices"),
            { itemTemplate: makeItemTemplate(false) }
          )
        );

       /**
       * 
       * Template for "Personnal" element
       * 
       */
        myDiagram.nodeTemplateMap.add("Personnal",

        $(go.Node, "Spot",
          { locationSpot: go.Spot.Center },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          { selectable: true, selectionAdornmentTemplate: nodeSelectionAdornmentTemplate, textEditable: true },
          new go.Binding("angle").makeTwoWay(),
          {
            click: function(e, obj) { if(clickedTool) {clickedTool = false; hideToolTip(); }else {clickedTool = true; showToolTip(e, obj);}},
            mouseOver: function(e, obj) { if(clickedTool) { showToolTip(e, obj);}},   
            mouseLeave: function(e, obj) {  hideToolTip(); }
          },
          // the main object is a Panel that surrounds a TextBlock with a Shape
          $(go.Panel, "Auto",
            { name: "PANEL" },
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            $(go.Shape, "FramedRectangle", 
              new go.Binding("desiredSize", "size2", go.Size.parse),
              new go.Binding("figure"),
              new go.Binding("fill"),
              new go.Binding("strokeWidth")),
            $(go.TextBlock,
              {
                font: "bold 11pt Helvetica, Arial, sans-serif",
                margin: 8,
                maxSize: new go.Size(160, NaN),
                wrap: go.TextBlock.WrapFit,
                editable: true
              },
              new go.Binding("text").makeTwoWay())
          ),
          $(go.Panel, "Vertical",
            { name: "LEFTPORTS", alignment: new go.Spot(0, 0.4, 0.5, 7) },
            new go.Binding("itemArray", "inservices"),
            { itemTemplate: makeItemTemplate(true) }
          ),
          $(go.Panel, "Vertical",
            { name: "RIGHTPORTS", alignment: new go.Spot(1, 0.4, 0.5, 7) },
            new go.Binding("itemArray", "outservices"),
            { itemTemplate: makeItemTemplate(false) }
          )
        ));


       /**
       * 
       * Template for "Data" element
       * 
       */
        myDiagram.nodeTemplateMap.add("Data",
        $(go.Node, "Spot",
          { locationSpot: go.Spot.Center },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          { selectable: true, selectionAdornmentTemplate: nodeSelectionAdornmentTemplate, textEditable: false },
          $(go.Shape, "FramedRectangle",
            { desiredSize: new go.Size(100, 55) },
            new go.Binding("figure"),
            new go.Binding("fill"),
            new go.Binding("strokeWidth")),
            $(go.Shape, "Rectangle",
            { fill: "white" },
            new go.Binding("desiredSize", function(n) {

                if(n.name == "blank") {
                  return go.Size.parse("0 0");
                }

                return go.Size.parse((100-16)+" "+(55-16));
              
            }),
            new go.Binding("strokeWidth")),
            $(go.TextBlock,
              {
                font: "bold 11pt Helvetica, Arial, sans-serif",
                margin: 8,
                maxSize: new go.Size(80, NaN),
                wrap: go.TextBlock.WrapFit,
                editable: true
              },
              new go.Binding("text").makeTwoWay()),
          $(go.Panel, "Vertical",
            { name: "RIGHTPORTS", alignment: new go.Spot(1, 0.4, 0, 7) },
            new go.Binding("itemArray", "outservices"),
            { itemTemplate: makeItemTemplate(false) }
          )
        ));

        /**
         * Template for "Link" element
         */
        myDiagram.linkTemplate =
        $(go.Link,
          {
            layerName: "Background",
            corner: 15,
            reshapable: true,
            resegmentable: true,
            fromSpot: go.Spot.RightSide,
            toSpot: go.Spot.LeftSide,
            routing: go.Link.AvoidsNodes,
            toShortLength: 4,
            selectable: true,
            relinkableFrom: true, 
            relinkableTo: true, 
            reshapable: true
          },
          // make sure links come in from the proper direction and go out appropriately
          new go.Binding("fromSpot", "fromSpot", go.Spot.parse),
          new go.Binding("toSpot", "toSpot", go.Spot.parse),
          new go.Binding("points").makeTwoWay(),
          // mark each Shape to get the link geometry with isPanelMain: true
          $(go.Shape, { isPanelMain: true, stroke: "gray", strokeWidth: 10, opacity: 0 }),
          $(go.Shape, { isPanelMain: true, stroke: "white", strokeWidth: 3, name: "PIPE", strokeDashArray: [20, 40] })
        );

      load();  // load an initial diagram from some JSON text

        function createPaletteType(paletteName, data) {

          var pal =
              $(go.Palette, paletteName,  // must name or refer to the DIV HTML element
                {
                  maxSelectionCount: 1,
                  nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
                  linkTemplate: // simplify the link template, just in this Palette
                    $(go.Link,
                      { // because the GridLayout.alignment is Location and the nodes have locationSpot == Spot.Center,
                        // to line up the Link in the same manner we have to pretend the Link has the same location spot
                        locationSpot: go.Spot.Center,
                        selectionAdornmentTemplate:
                          $(go.Adornment, "Link",
                            { locationSpot: go.Spot.Center },
                            $(go.Shape,
                              { isPanelMain: true, fill: null, stroke: "deepskyblue", strokeWidth: 0 }),
                            $(go.Shape,  // the arrowhead
                              { toArrow: "Standard", stroke: null })
                          )
                      },
                      {
                        routing: go.Link.Orthogonal,
                        curve: go.Link.JumpOver,
                        corner: 5,
                        toShortLength: 4
                      }
                    ),
                  model: new go.GraphLinksModel(data)
                });

            return pal;

        }

        var down = true;

        /**
         * App loop for animate the link
         */
        function loop() {
          var diagram = myDiagram;
          setTimeout(function() {
            var oldskips = diagram.skipsUndoManager;
            diagram.skipsUndoManager = true;
            diagram.links.each(function(link) {
              var shape = link.findObject("PIPE");
              if(shape != null) {
                if(link.fromNode != null) {
                  shape.stroke = link.fromNode.data.fill;
                  var off = shape.strokeDashOffset - 3;
                  // animate (move) the stroke dash
                  if(link.toNode != null) {
                    shape.strokeDashOffset = (off <= 0) ? 60 : off;
                  }
                }
              }
            });
            diagram.skipsUndoManager = oldskips;
            loop();
          }, 60);
        }

        /**
         * 
         */
        resetPalette = function(paletteName) {

          var projectDiagramDiv = document.getElementById(paletteName);
          var projectDiagram = go.Diagram.fromDiv(projectDiagramDiv);
          if(projectDiagram){
            projectDiagram.div = null;
          }
        }

        /**
         * 
         */
        createPalette = function(paletteName, data) {

          for (let i = 0; i < data.length; i++) {
            data[i].key = Math.random().toString(10).substr(2,10);
          }

          var pal =
            $(go.Palette, paletteName,  // must name or refer to the DIV HTML element
              {
                maxSelectionCount: 1,
                nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
                linkTemplate: // simplify the link template, just in this Palette
                  $(go.Link,
                    { // because the GridLayout.alignment is Location and the nodes have locationSpot == Spot.Center,
                      // to line up the Link in the same manner we have to pretend the Link has the same location spot
                      locationSpot: go.Spot.Center,
                      selectionAdornmentTemplate:
                        $(go.Adornment, "Link",
                          { locationSpot: go.Spot.Center },
                          $(go.Shape,
                            { isPanelMain: true, fill: null, stroke: "deepskyblue", strokeWidth: 0 }),
                          $(go.Shape,  // the arrowhead
                            { toArrow: "Standard", stroke: null })
                        )
                    },
                    {
                      routing: go.Link.Orthogonal,
                      curve: go.Link.JumpOver,
                      corner: 5,
                      toShortLength: 4
                    }
                  ),
                model: new go.GraphLinksModel(data)
              });

            return pal;
          }

          <?php

            foreach ($TOOLS_TITLE as $keyT => $title) {
                echo 'myPalette_'.$keyT.' = createPalette("myPaletteDiv_'.$keyT.'", MBB_TOOLS["'.$keyT.'"]);';
            }

          ?>

          jQuery("#accordion").accordion({
            collapsible: true,
            active: false,
            activate: function(event, ui) {

              <?php

                echo 'myPalette_'.$keyT.'.requestUpdate();';

              ?>

            }
          });

          <?php

          foreach ($DATA_TITLE as $keyT => $title) {
              echo 'myPaletteType_'.$keyT.' = createPaletteType("myPaletteDivType_'.$keyT.'", MBB_DATA["'.$keyT.'"]);';
          }

          ?>

          jQuery("#accordion2").accordion({
            collapsible: true,
            active: false,
            activate: function(event, ui) {
              <?php

              echo 'myPaletteType_'.$keyT.'.requestUpdate();';

              ?>
            }
          });

        function hideToolTip() {
          var box = document.getElementById("infoBoxHolder");
          box.innerHTML = "";
        }

        function showToolTip(e, obj) {
          
          if (obj !== null) {
            var node = obj;

            if(node.category === "Tool") {
              updateInfoBoxTool(e.viewPoint, node.data);
            }

          }
          else {
            var box = document.getElementById("infoBoxHolder");
            box.innerHTML = "";
          }
      
        }

        /**
         * Update the info box of the Tool
         */
        function updateInfoBoxTool(mousePt, data) {
          var box = document.getElementById("infoBoxHolder");
          box.innerHTML = "";
          var infobox = document.createElement("div");
          infobox.id = "infoBox";
          box.appendChild(infobox);

          var jtool = new SUBWAW.JTool(data);
          jtool.addSteps();
          jtool.addInputs(data);
          jtool.addOutputs(data);

          var jbox = new SUBWAW.JToolInfoBox(jtool);
          jbox.addVersionElement(infobox);
          jbox.addStepsElement(infobox);
          jbox.addPorts(infobox, "IN");
          jbox.addPorts(infobox, "OUT");
          jbox.addDescriptionElement(infobox);

          box.style.left = mousePt.x + 450 + "px";
          box.style.top = (mousePt.y + 125) + "px";
        }


        /**
         * Valid the link beetween two node
         * 
         * @param {Object} fromnode
         * @param {Object} fromport
         * @param {Object} tonode
         * @param {Object} toport
         * 
         * @return bool
         */
        function validLink(fromnode, fromport, tonode, toport) {


          if(fromnode == null || tonode == null) {
            return false;
          }

          if(fromport.ptype == toport.ptype) {
            return false;
          }

          var fdata = fromnode.data;
          var tdata = tonode.data;

          if(fromnode == null || fromport == null || tonode == null || toport == null) {
            return false;
          }

          if(fdata.name == tdata.name) {
            return false;
          }

          // TODO check if this condition is always needed
          if((fdata.category == "Step" || fdata.category == "StepNull") && tdata.category == "Tool"){

            var jsonValue =  JSON.parse(myDiagram.model.toJson());

            var links = jsonValue["linkDataArray"];

            var toolsStep = links.filter(function(d) { return (d.from === fdata.key) && (d.fromPort === "linkOut") && (d.toPort !== "");});

            if(toolsStep.length == 0) {
              return MBB_STEP_CATEGORY[fdata.name].includes(tdata.name);
            }

            return false;
          }

          var acceptList = toport.list; // check if input port accept list

          var jsonValue =  JSON.parse(myDiagram.model.toJson());
          var links = jsonValue["linkDataArray"];
          var sizeConnexion = links.filter(function(d) { return (d.to === tdata.key) && (d.toPort === toport.portId);}).length;

          if(tdata.name != "multiqc") {
            if(!acceptList && sizeConnexion > 0) {
                return false;
            }
          }

          if(fromport.type == "*" || toport.type == "*") {
            return true;
          }
          else if(fromport.type == "reads" && toport.type == "fq.gz") {
            return true;
          }
          else if(fromport.type == "fq.gz" && toport.type == "reads") {
            return true;
          }
          else if(fromport.type == "contigs" && toport.type  == "fasta_file") {
            return true;
          }
          else if(fromport.type == "fasta_file" && toport.type  == "contigs") {
            return true;
          }
          // else if(toport.type == "gstacks_dir" && fromnode.data.name == "gstacks_denovo") {
          //   return true;
          // }
          // else if(toport.type == "gstacks_dir" && fromnode.data.name == "gstacks_refMap") {
          //   return true;
          // }
          else if(toport.type == "ustacks_dir" && fromnode.data.name == "ustacks") {
            return true;
          }
          else if(toport.type == "cstacks_dir" && fromnode.data.name == "cstacks") {
            return true;
          }
          else if(toport.type == "sstacks_dir" && fromnode.data.name == "sstacks") {
            return true;
          }
          else if(toport.type == "transrate_dir" && fromnode.data.name == "transrate") {
            return true;
          }
          else if(!Array.isArray(fromport.type) && !Array.isArray(toport.type)) {
            return (fromport.type == toport.type);
          }

          if(!Array.isArray(fromport.type)) {
            if(fromport.type == "reads") {
              fromport.type = ["reads", "fq.gz"];
            }
            else {
              fromport.type = [fromport.type];
            }
          }

          if(!Array.isArray(toport.type)) {
            if(toport.type == "reads") {
              toport.type = ["reads", "fq.gz"];
            }
            else {
              toport.type = [toport.type];
            }
          }

          return fromport.type.some(r=> toport.type.includes(r));
        }

        // only allow new links between ports of the same color
        myDiagram.toolManager.linkingTool.linkValidation = validLink;

        // only allow reconnecting an existing link to a port of the same color
        myDiagram.toolManager.relinkingTool.linkValidation = validLink;          

        // Cycle not allowed
        myDiagram.validCycle = go.Diagram.CycleNotDirected;

      // initialize Overview
      /*var myOverview =
        $(go.Overview, "myOverviewDiv",
          {
            observed: myDiagram,
            contentAlignment: go.Spot.Top
          });*/

      loop();
    } //END init

    
    /**
     * Get the key of the childs of the current node
     * 
     * @param {Array} nodes : The nodes array
     * @param {int} key : The key of the current node
     * @param {Array} links : The links array
     * 
     */
    function getChildKey(nodes, key, links) {
      var results = [];
      for (const i in nodes) {

        var node = nodes[i];

        if(key != node.key) {
          continue;
        }

        var inputs = node.outservices;

        var checkDeep = true;

        for (const k in inputs) {

          var nameInput = inputs[k].value;

          var linksInputs = links.filter(function(d) { return (d.from === node.key) && (nameInput == d.fromPort)});



          for(l in linksInputs) {

            var LI = linksInputs[l];

            var originNodeTools = nodes.filter(function(d) { return d.key === LI.to});
            

            for (const l in originNodeTools) {
              var onode = originNodeTools[l];
              results.push(onode.key);
            }
          }
          
        }
      }
      return [...new Set(results)];
    }

    /**
     * Increment the deep of the child of the current node
     * 
     * @param {Array} deeps : The deeps array
     * @param {Array} nodes : The nodes array
     * @param {int} key : The key of the current node
     * @param {Array} links : The links array
     * 
     */
    function incrementDeep(deeps, nodes, key, links) {
      var childs = getChildKey(nodes, key, links);
      for(i in childs) {
        deeps[childs[i]] += deeps[key] + 1;
        incrementDeep(deeps, nodes, childs[i], links);
      }
    }

    const MESSAGE_TYPE = {
        DEFAULT: 'black',
        WARNING: 'orange',
        ERROR: 'red'
    }

    /**
     * Print message in the console
     * 
     * @param {string} msg : The message to print
     * @param {MESSAGE_TYPE} type : The type of message.
     */
    function printToConsole(msg, type = MESSAGE_TYPE.DEFAULT) {
      document.getElementById("consolePreview").innerHTML += "<p style='color: " + type + "' > " + msg + "</p><br/>";
    }

    /**
     * Transform Workflow info YAML content
     * 
     * @return {string}
     */
const unique = (value, index, self) => {
  return self.indexOf(value) === index
}
    function transform3(preview, run) {

      var myImage = myDiagram.makeImage({
        scale: 1,
        background: "white",
        type: "image/png"
      });
      
      if(run) {
        document.getElementById("pngWorkflowRun").value = myImage.src;
      }
      else {
        document.getElementById("pngWorkflow").value = myImage.src;
      }

      var result = "{\n";

      var alertMessage = "";

      var runid = "";

      if(run) {
        runid += "Run";
      }

      var name = document.getElementById("nameW"+runid).value.replace("/ /g","_");
      if(name == "") {
        alertMessage += "name field is required.\n";
      }
      result += "name: \"" + escapeHtml(name) + "\",\n";

      var docker_name = name.toLowerCase().replace(/\s/g, '_');
      result += "docker_name: \"" + escapeHtml(docker_name) + "\",\n";

      var description = document.getElementById("descriptionW"+runid).value;
      result += "description: \"" + escapeHtml(description) + "\",\n";

      var version = document.getElementById("versionW"+runid).value;
      if(version == "") {
        alertMessage += "version field is required.\n";
      }
      result += "version: \"" + escapeHtml(version) + "\",\n";

      var author = document.getElementById("authorW"+runid).value;
      if(author == "") {
        author = "MBB";
      }
      result += "author: \"" + escapeHtml(author) + "\",\n";

      if(alertMessage != "" && preview == false) {
        alert(alertMessage);
        return "";
      }

      // options :
      result += "options: [\n"

      result += "    {\n"
      result += "    name: \"results_dir\",\n"
      result += "    type: \"output_dir\",\n"
      result += "    value: \"/Results\",\n"
      result += "    label: \"Results directory: \",\n"
      result += "    volumes:  [Results: \"/Results\"]\n"
      result += "    },\n"

      result += "],\n"
      // end options

      const jsonValue =  JSON.parse(myDiagram.model.toJson());
      const nodes = jsonValue["nodeDataArray"];
      const links = jsonValue["linkDataArray"];

      raw_input = [];

      if(nodes.length == 0) {
        printToConsole("Error: you don't have nodes in your Workflow", MESSAGE_TYPE.ERROR);
        return "";
      }

      if(links.length == 0) {
        printToConsole("Error: you don't have links in your Workflow", MESSAGE_TYPE.ERROR);
        return "";
      }

      // step in
      result += "steps_in: [\n"

      var steps = [];
      var steps2 = [];

      var stepsOrder = [];
      var stepsTitleOrder = [];

      var stepsToolOrder = [];

      
      for (const i in nodes) {

        var node = nodes[i];

        if(node.category != "Tool") {
          continue;
        }

        var stepName = "";

        for( var key in MBB_STEP_CATEGORY ) {
            var values = MBB_STEP_CATEGORY[key];

            if(values.includes(node.name)) {

              var keyStep = key;

              if(steps[keyStep] == undefined) {
                steps[keyStep] = 0;
              }

              steps[keyStep] = steps[keyStep] + 1; 
              
              stepName = keyStep;

              //if(steps[keyStep] > 1) {
                stepName += "_" + steps[keyStep];
             // }
            }
        }



        if(stepName == "") {
          stepName = "step_" + node.name;
        }

        if(node.category == "Tool") {
          steps2[node.key] = stepName;
        }

      }

      var deeps = [];
      var tools = [];

      for (const i in nodes) {
        var node = nodes[i];
        var identifier = node.key.toString();
        deeps[identifier] = 0;
        tools[identifier] = node.name;

      }

      for (const i in nodes) {

        var node = nodes[i];

        var inputs = node.outservices;

        var checkDeep = true;

        for (const k in inputs) {

          var nameInput = inputs[k].value;

          var linksInputs = links.filter(function(d) { return (d.from === node.key) && (nameInput == d.fromPort)});

          if(linksInputs.length > 0) {
            linksInputs = linksInputs[0];

            var originNodeTools = nodes.filter(function(d) { return d.key === linksInputs.to});

            for (const l in originNodeTools) {
               var onode = originNodeTools[l];
               //deeps[onode.name] += deeps[node.name] + 1;
               incrementDeep(deeps, nodes, node.key, links);
            }
          }
        }
      }

      var sortedNodes = [];

      deeps = getSortedKeys(deeps);

      for (const i of deeps) {
        var node = nodes.filter(function(d) { return d.key === i})[0];
        if(node != undefined) {
          sortedNodes.push(node);
        }
      }

      var params_equals = "";

      var dataNodeAlreadyParse = [];

      for (const i in sortedNodes) {

        var node = sortedNodes[i];

        if(node.category != "Tool") {
          continue;
        }

        var stepName = steps2[node.key];

        stepsOrder.push(stepName);
        stepsTitleOrder.push(steps2[node.key]);

        var nameTool = node.name.replace("_SE","").replace("_PE","");

        

        //stepsToolOrder.push(nameTool);
		stepsToolOrder.push(node.name);

        var stepInValue = "{ step_name: " + "\"" + stepName + "\", tool_name: \"" + nameTool + "\", rule_name: \"" + node.name + "\",\n    params: [\n"


        var inputs = node.inservices;

        for (const k in inputs) {

          var nameInput = inputs[k].value;

          var linksInputs = links.filter(function(d) { return (d.to === node.key) && (nameInput == d.toPort)});

          if(linksInputs.length > 0) {
            //linksInputs = linksInputs[0];


            for (const m in linksInputs) {

              var currentLink = linksInputs[m];

              var originNodeTool = nodes.filter(function(d) { return d.key === currentLink.from})[0];



              var oc = originNodeTool.name;
              var on = currentLink.fromPort;
              var os = "";

              // TODO fix du pauvre
              if(originNodeTool.category == "Data") {

                on = originNodeTool.name;
                oc = currentLink.fromPort;

                if(oc.includes("raw_")) {
                    if(inputs[k].from == "parameter" && oc.includes("raw_")) {
                        //on va tout simplement ignorer ce raw_data pour qu'il n'aparaisse pas la page des global param.
                    }
                    else {
                      raw_input.push(oc);
                    }
                }

                os = "input";
              }
              else {
                os = steps2[originNodeTool.key];
              }

              if(oc == "raw_reads" && on == "reads") {
                on = currentLink.toPort;
              }
              if(oc == "raw_reads_SE" && on == "reads_SE") {
                on = currentLink.toPort ;
              }
              if(oc == "raw_reads_PE" && on == "reads_PE") {
                on = currentLink.toPort ;
              }
              // on ignore les ports qui from de parameter et sont en lien avec une brique raw
              if(inputs[k].from == "parameter" && oc.includes("raw_")) {
                stepInValue += "        #{ input_name: " + currentLink.toPort + ", origin_command: " + oc + ", origin_name: " + on + ", origin_step: " + os + "}, # This input will be read from the params file (see documentation)\n"
              }
              else {
                stepInValue += "        { input_name: " + currentLink.toPort + ", origin_command: " + oc + ", origin_name: " + on + ", origin_step: " + os + "},\n"
              }

              if (inputs[k].from == "parameter" && originNodeTool.category == "Tool"){
                params_equals += "        { remove: " + stepName + "__" + nameTool + "_" + currentLink.toPort + "},\n";
              }

              if(oc != "raw_reads" && oc != "raw_reads_SE" && oc != "raw_reads_PE" && originNodeTool.category == "Data" && !dataNodeAlreadyParse.includes(originNodeTool.key)) {

                dataNodeAlreadyParse.push(originNodeTool.key);
    
                var linksToChild = links.filter(function(d) { return (d.from === originNodeTool.key) });

                if(linksToChild.length > 1) {

                  var subChildNode = [];

                  for (const sm in linksToChild) {
                      var currentLinkChild = linksToChild[sm];
                      var originNodeToolChild = nodes.filter(function(d) { return d.key === currentLinkChild.to});
                      subChildNode.push(originNodeToolChild[0]);
                  }

                  var paramA = subChildNode[0];
                  var currentLinkChild = links.filter(function(d) { return (d.to === paramA.key) && (d.from == originNodeTool.key)})[0];
                  var valuePA = steps2[paramA.key] + "__" + paramA.name + "_" + currentLinkChild.toPort;

                  for (let ip = 1; ip < subChildNode.length; ip++) {
                    const paramB = subChildNode[ip];
                    var currentLinkChild2 = links.filter(function(d) { return (d.to === paramB.key) && (d.from == originNodeTool.key) })[0];
                    var valuePB = steps2[paramB.key] + "__" + paramB.name + "_" + currentLinkChild2.toPort;

                    params_equals += "        { param_A: \"" + valuePA + "\", param_B:   \"" + valuePB + "\"},\n";
                  }
                }
              }
            }
          }
        }

        stepInValue += "    ]\n},\n";
    
        result += stepInValue;
      }

      result += "],\n"
      // end step in

      // steps :
      result += "steps: [\n"

      for (let i = 0; i < stepsOrder.length; i++) {
        const stepName = stepsOrder[i];
        const toolName = stepsToolOrder[i].replace("_SE","").replace("_PE","");
		
        var stepValue = "    { title: \"" + stepsTitleOrder[i] + "\", name: " + "\"" + stepName + "\", tools: ["+ toolName + "], default: \"" + toolName  + "\" , rule_name: \" "+ stepsToolOrder[i] + "\" },\n";
        result += stepValue;
      }

      result += "],\n"
      // end step

      let raw_input_filter = [...new Set(raw_input)];
      uniqueraw_input = raw_input.filter(unique)
      if(uniqueraw_input.length > 0) {
        result += "input: [";
        for (let i = 0; i < uniqueraw_input.length; i++) {
          let ri = uniqueraw_input[i];
          if(MBB_TOOLS_RAW_INPUT.includes(ri)) {
            result += "\"" + ri + "\",";
            //break;
          }
        }
        result += "],\n";
      }

      // params equals
      result += "params_equals: [\n"
      result += params_equals
      result += "\n"
      result += "]"
      // end params equals

      result += "\n}\n";

      return result;
    }

    /**
     * Export Workflow in PNG file format
     * 
     * @param {object} linkElement : The element who call this function
     */
    function exportPng(linkElement) {
      var myImage = myDiagram.makeImage({
        scale: 1,
        background: "white",
        type: "image/png"
      });
      linkElement.href = myImage.src;
    }

    /**
     * Import Workflow from database
     * 
     * @param {int} name : The name of the Workflow
     * 
     */
    function importGraphic(name) {

      if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
      } else {
          xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
      }

      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              //var value = unescape(this.responseText);
              var value = "";

              var workflow = JSON.parse(this.responseText);

              if(workflow.length > 0) {
                  value = workflow[0]["json"];
              }

              myDiagram.model = go.Model.fromJson(value);
              loadDiagramProperties(); 
              generateWorkflow(true, false);
          }
      };

      xmlhttp.open("GET","api/workflows.php?name="+name,true);
      xmlhttp.send();
    }

    function exportJSON(linkElement) {
      saveDiagramProperties();  // do this first, before writing to JSON
      var json = myDiagram.model.toJson();
      var objjson = JSON.parse(json);
      objjson.version = <?php echo Conf::$VERSION; ?>;
      myDiagram.isModified = false;
      var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(objjson));
      linkElement.setAttribute("href", "data:"+data);
    }

    /**
     * Clean Workflow
     */
    function clean() {
      document.getElementById("workflowPreview").value = "";
      load(true);
    }

    /**
     * Load Workflow
     */
    function load(clean = false) {
      var value = "";
      if(!clean) {
        value = <?php echo $loadJSON; ?>;
      }
      else {
        value = <?php echo $defaultJSON; ?>;
      }
      myDiagram.model = go.Model.fromJson(value);
      loadDiagramProperties();  // do this after the Model.modelData has been brought into memory
    }

    /**
     * Save diagram properties
     */
    function saveDiagramProperties() {
      myDiagram.model.modelData.position = go.Point.stringify(myDiagram.position);
    }

    /**
     * Load diagram properties
     */
    function loadDiagramProperties(e) {
      // set Diagram.initialPosition, not Diagram.position, to handle initialization side-effects
      var pos = myDiagram.model.modelData.position;
      if (pos) myDiagram.initialPosition = go.Point.parse(pos);
    }

    /**
     * Event when user click on "Download" button
     */
    function downloadWorkflow() {

      var previewWorkflow = generateWorkflow(false, false);

      if(previewWorkflow) {
        //console.log("generate ok");
        //$('#'+id).modal('hide');
        var json = myDiagram.model.toJson();

        var objjson = JSON.parse(json);
        objjson.version = <?php echo Conf::$VERSION; ?>;
        jsonText = JSON.stringify(objjson);
        if (jsonText != "")
        {
        document.getElementById("jsonWorkflow").value = jsonText;
        } 
        document.getElementById("formWorkflow").submit();
      }
    }

    function hideSpinner() {
      document.getElementById("nameWRun").value = "workflow_"+getRandomArbitrary(10000, 99999);
      $("#runSpinner").hide();
      $("#runButton").prop("disabled", false);
    }

    function showSpinner() {
      $("#runSpinner").show();
      $("#runButton").prop("disabled", true);
    }

    /**
     * Event when user click on "Run" button for run workflow.
     * 
     */
    function runWorkflow() {

      var previewWorkflow = generateWorkflow(false, true);

      if(previewWorkflow) {

        var json = myDiagram.model.toJson();
        var objjson = JSON.parse(json);
        objjson.version = <?php echo Conf::$VERSION; ?>;
        document.getElementById("jsonWorkflowRun").value = JSON.stringify(objjson);
        document.getElementById("jsonWorkFlow").value = JSON.stringify(objjson);
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
        }

        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var value = JSON.parse(this.responseText);
                //document.getElementById("mySavedModel").value = value;
                console.log(this.responseText);
                $('#runProject').modal('toggle');

                var message = "";
                var publicIP = "127.0.0.1";
                var shiny_port = "<?php echo Conf::$SHINY_PORT ; ?>";
                if(value["stderr"] == "cool") {
                 
                 IP="<?php $IP=shell_exec("wget -qO- https://ipinfo.io/ip"); $IP = str_replace(["\n", "\r"], "", $IP); echo $IP; ?>";
                 if(IP!="") {publicIP = IP;}
                 message = "Your workflow is now running <a href = 'http://" + publicIP + ":" + shiny_port + "/' target='_blank'>here</a>.";
                }
                else {
                  message = "Your have to stop a running workflow to free the port 3838 before.";
                }

                document.getElementById("workflowRunning").innerHTML = message;
                $('#runSuccessModal').modal();
                
            }
        };

        var params = "";

        var name = document.getElementById("nameWRun").value;
        var description = document.getElementById("descriptionWRun").value;
        var version = document.getElementById("versionWRun").value;
        var author = document.getElementById("authorWRun").value;
        var png = document.getElementById("pngWorkflowRun").value;
        var preview = document.getElementById("previewWorkflowRun").value;
        var json = document.getElementById("jsonWorkflowRun").value;

        var user = document.getElementById("userWRun").value;
        var passwd = document.getElementById("passWRun").value;

        params += "action=run&";
        params += "name="+name+"&";
        params += "description="+description+"&";
        params += "version="+version+"&";
        params += "author="+author+"&";
        params += "png="+png+"&";
        params += "preview="+preview+"&";
        params += "json="+json+"&";
        params += "user="+user+"&";
        params += "passwd="+passwd;

        xmlhttp.open("POST","action/action_workflow.php", true);
        xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xmlhttp.send(params);
        showSpinner();
      }
    }

    /**
     * Generate yaml for Workflow
     * 
     * @param {bool} preview If you generate workflow for preview event
     * @param {bool} run If you generate workflow for run event
     * 
     * @return true if no error, otherwise false.
     */
    function generateWorkflow(preview, run) {
      var content = transform3(preview, run);
      
      if(content != "") {
        if(preview) {
			document.getElementById("workflowPreview").value = content;
        }
        else {
          if(run) {
            document.getElementById("previewWorkflowRun").value = content;
          }
          else {
            document.getElementById("previewWorkflow").value = content;
          }
        }
        return true;
      }
      else {
        document.getElementById("console-tab").click();
        document.getElementById("console-tab").focus();
        var consoleOutput = document.getElementById("consolePreview");
        consoleOutput.scrollTop = consoleOutput.scrollHeight;
        return false;
      }
    }

	function editWorkflow() {
      var content = transform3(true, false);

      if(content != "") {
				var myImage = myDiagram.makeImage({
					scale: NaN,
          maxSize: new go.Size(1200,800),
					background: "white",
					type: "image/png"
				});
    		document.getElementById("yamlText").value = content;
				document.getElementById("helperpng").value = myImage.src;
        document.getElementById("pngWorkflowRun").value = myImage.src;
        var json = myDiagram.model.toJson();
        var objjson = JSON.parse(json);
        objjson.version = <?php echo Conf::$VERSION; ?>;
        document.getElementById("jsonWorkFlow").value = JSON.stringify(objjson);
        document.getElementById("jsonWorkflowRun").value = JSON.stringify(objjson);

      
      	document.getElementById('myForm').submit();
         
        }
        
        return true;
      
    }


    init();
    reportWindowSize();
    
    //Preload all nodes are deactivate
    //searchNodes('myPaletteDiv', 1);
    //searchNodes('myPaletteDivType', 0);

    <?php 
      if($import && ($workflow != NULL)) {
        echo "importGraphic('".$workflow->name."');";
      }
      else {
        echo "load();";
      }
    ?>

</script>

</body>
</html>
