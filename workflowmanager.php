<?php

session_start ();

require_once( "./inc/php/buildHeader.php");

require_once("./dao/DBquery.php");

require_once("./tool.php");

$db = new DBqueryLite();

$workflows = getYAMLWorkflows();

?>

<div class="container-fluid">

<div class="row justify-content-md-center text-center">
    <div class="col-sm-2">
        <div class="card border-primary sm-2">
        <div class="card-header font-weight-bold text-white bg-primary"><h4>Workflows</h4></div>
        <div class="card-body text-primary">
            <h3 class="card-title"><?php echo count($workflows); ?></h3>      
        </div>
        </div>
    </div>
</div>


<br/><br/>

<div class="row">
    <div class="col-12">
        <form action="./action/action_workflow.php" method="post">

            <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="download_zip">

            <!-- <button type="submit" class="btn btn-info">Download selected workflows</button>-->
            <a class="btn btn-success" href="./workflow.php"> <i class="fas fa-plus-circle"></i> Create new Workflow </a>

            <br/><br/>

            <table id="Table_Container" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <!--<th>Choose</th>-->
                        <th>Name</th>
                        <th>DockerName</th>
                        <th>Author</th>
                        <th>Label</th>
                        <th>Description</th>
                        <th>Version</th>
                        <th>Git</th>
                        <th>JSON version</th>
                        <!--<th>Creation</th>
                        <th>Update</th>-->
                        <th class="col-1"></th>
                    </tr>
                </thead>
                <tbody id="TableWorkflow">

                <?php

                foreach ($workflows  as $workflow) {

                    $workflow->escape2($db);
                    
                    echo '<tr>';
                    //echo '<td><input type="checkbox" value="'.$workflow->name.'" name="idworkflows[]"></td>';
                    echo '<td>' . $workflow->name . '</td>';
                    echo '<td>' . $workflow->docker_name . '</td>';
                    echo '<td>' . $workflow->author . '</td>';

                    //$labels = $db->getLabelWithWorkflowId($workflow->ID);
                    $labels = [];
                    array_push($labels, new Label(0, "MBB", "2ecc71", 1));
                    array_push($labels, new Label(0, "stable", "9b59b6", 2));

                    echo '<td>';

                    $index = 0;

                    foreach ($labels as $label) {
                        echo '<span class="badge" style="background:#'.$label->color.'">'.$label->name.'</span>';

                        if($index == 1) {
                            echo '<br/>';
                            $index = 0;
                        } else {
                            $index++;
                        }       
                    }
                    
                    echo '</td>';                   
                    echo '<td>' . $workflow->description . '</td>';
                    echo '<td>' . $workflow->version . '</td>';

                    echo '<td>';

                    if($workflow->git != "") {
                        echo "<a href='".$workflow->git."'>git</a></br>";
                    }
                    
                    echo '</div></td>';

                    echo '<td class="text-center">';

                    $path_json = Conf::$PATH_YAML_WORKFLOWS . $workflow->name . "/" . $workflow->name . ".json";

                    if(file_exists($path_json)) {
                        $graphics = json_decode(file_get_contents($path_json), true);

                        if(array_key_exists('version', $graphics)) {
                            if($graphics['version'] < Conf::$VERSION) {
                                echo '<span class="badge badge-warning"><i class="fas fa-code-branch"></i>&nbsp;v'.$graphics->version.'</span>';
                            }
                            else {
                                echo '<h3><span class="badge badge-success"><i class="fas fa-code-branch"></i>&nbsp;v'.$graphics->version.'</span></h3>';
                            }
                        }
                        else {
                            echo '<span class="badge badge-danger" style="background: #e74c3c">error no version</span>';
                        }              
                    }

                    echo '</td>';

                    echo '<td>';

                    // echo '<div class="btn-group" ><button type="button" class="btn btn-dark btn-sm" onclick="showYamlWorkflow(\''.$workflow->name.'\')"><i class="far fa-eye"></i>&nbsp; YAML</button></div>';
                    
                    //echo '<div class="btn-group" ><a class="btn btn-primary btn-sm" href="./action/action_workflow.php?action=download&workflowid='.$workflow->ID.'">dowload</a></div>';
                    
                    if($workflow->author == $_SESSION['username'] || $_SESSION['manage'] ) {
                        //echo '<div class="btn-group"><a class="btn btn-warning btn-sm" href="./workflow.php?workflowid='.$workflow->ID.'&action=update" >update</a></div>';

                        //echo '<div class="btn-group"><a class="btn btn-danger btn-sm" href="./action/action_workflow.php?action=delete&workflowid='.$workflow->ID.'" >delete</a></div>';
                    }

                    if($workflow->graphicYAML != "") {
                        echo '<div class="btn-group"><a class="btn btn-info btn-sm" href="./workflow.php?workflowid='.$workflow->name.'&action=import" >Design WF</a></div>';
                    }

                    #echo '<div class="btn-group"><a target="_blank" class="btn btn-primary btn-sm" href="./api/workflows.php?name='.$workflow->name.'" ><i class="fas fa-cogs"></i>&nbsp; API</a></div>';
                    echo '<div class="btn-group"><a class="btn btn-primary btn-sm" href="./runworkflow.php?workflowid='.$workflow->name.'&action=RUN" ><i class="fas fa-cogs"></i>&nbsp;Run WF</a></div>';
                    

                    echo '</td>';

                    echo '</tr>';

                }


                ?>


                </tbody>
            </table>
        </form>
    </div>
</div>

    <!-- Start Modal Publish -->
    <div class="modal" id="modalShowWorkflow">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header">
                    <h4 class="modal-title">Workflow yaml</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <textarea type="text" class="form-control" id="valueWorkflowFile" name="value" value=""></textarea>
                </div>
                <div class="modal-footer">
                    <!--<button type="submit" class="btn btn-success">Import</button>-->
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>  
    <!-- End Modal Publish -->

</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"  crossorigin="anonymous"></script>
<script src = "./inc/js/workflow.js"></script>
