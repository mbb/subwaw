<?php

session_start ();

require_once "./inc/php/buildHeader.php";

require_once "./dao/DBquery.php";

$db = new DBqueryLite();

$tool = NULL;

$SAVE = FALSE;

if(isset($_GET['toolid'])) {

    $grade = $db->getGradeWithLogin($_SESSION['username']);

    $level = 999;

    if($grade != NULL) {
        $level = $grade->level;
    }

    $tool = $db->getToolWithId($_GET['toolid']);

    if($tool != NULL) {
        if(($tool->author == $_SESSION['username']) || ($_SESSION['username'] != "admin") || ($level <= 1)) {
            $SAVE = TRUE;
        }    
    }
}

$TID           = "";
$name          = "";
$description   = "";
$version       = "";
$website       = "";
$git           = "";
$documentation = "";
$article       = "";
$multiQC       = "custom";
$install       = "";
$yaml          = "";
$author        = "";
$creationDate  = date("Y-m-d");
$updateDate    = "";
$tags          = "";

$labels = array();

$NROW = 3;

//TODO clean this.

if($tool != NULL) {
    $TID           = $tool->TID;
    $name          = $tool->name;
    $description   = $tool->description;
    $version       = $tool->version;
    $website       = $tool->website;
    $git           = $tool->git;
    $documentation = $tool->documentation;
    $article       = $tool->article;
    $multiQC       = $tool->multiQC;
    $install       = $tool->install;
    $yaml          = $tool->yaml;
    $author        = $tool->author;
    $creationDate  = $tool->creationDate;
    $updateDate    = $tool->updateDate;
    $tags          = $tool->tags;
    $NROW = 30;

    $labelO = $db->getLabelWithToolId($tool->ID);

    foreach ($labelO as $label) {


        array_push($labels, $label->name);
    }


}

$path_waw_tools = "../waw/tools/";

$path_global = $path_waw_tools."global.yaml";

$global = yaml_parse_file($path_global);

$categories_title = [];

if(array_key_exists("categories", $global)) {

    $ycategories = $global["categories"];

    foreach ($ycategories as $yc) {
        $categories_title[$yc["name"]] = $yc["title"];
    }    
}

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-3">
            <div class="col-12">
                <div class="card border border-dark">
                    <div class="card-header bg-dark text-light">
                        Tool :
                    </div>
                    <div class="card-body">
                    <form id="downloadTool" action="./action/action_tool.php" method="post">
                        <input id="actionTool" type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="download">
                        <input id="yamlTool" type="text" class="form-control" style="display: none;" placeholder="action" name="yaml" value="">
                        <div class="form-group">
                            <label for="IDBrick" class="font-weight-bold">ID <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                            <input type="text" class="form-control" id="IDBrick" name="tid" value = "<?php echo $TID; ?>" required>
                            <small class="form-text text-muted">No space in ID value.</small>
                        </div>
                        <div class="form-group">
                            <label for="NameBrick" class="font-weight-bold">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                            <input type="text" class="form-control" id="NameBrick" name="name" value = "<?php echo $name; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="CreationDate" class="font-weight-bold">Creation date :</label>
                            <input type="text" name="creationDate" class="form-control" id="CreationDate" value = "<?php echo $creationDate; ?>" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="DescriptionBrick" class="font-weight-bold">Description <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                            <textarea id="DescriptionBrick" name="description"  class="form-control" rows="3" maxlength="500" required><?php echo $description; ?></textarea>
                            <small class="form-text text-muted">500 characters maximum.</small>
                        </div>
                        <div class="form-group">
                            <label for="VersionBrick" class="font-weight-bold">Version <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                            <input type="text" name = "version" class="form-control" id="VersionBrick" value = "<?php echo $version; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="WebsiteBrick" class="font-weight-bold">Website :</label>
                            <input type="text" name="website" class="form-control" id="WebsiteBrick" value = "<?php echo $website; ?>">
                        </div>
                        <div class="form-group">
                            <label for="GitBrick" class="font-weight-bold">Git :</label>
                            <input type="text"name="git" class="form-control" id="GitBrick" value = "<?php echo $git; ?>">
                        </div>
                        <div class="form-group">
                            <label for="DocumentationBrick" class="font-weight-bold">Documentation :</label>
                            <input type="text" name="documentation" class="form-control" id="DocumentationBrick" value = "<?php echo $documentation; ?>">
                        </div>
                        <div class="form-group">
                            <label for="ArticleBrick" class="font-weight-bold">Article :</label>
                            <input type="text" name = "article" class="form-control" id="ArticleBrick" value = "<?php echo $article; ?>">
                        </div>
                        <div class="form-group">
                            <label for="MultiQCBrick" class="font-weight-bold">MultiQC <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                            <input type="text" name = "multiQC" class="form-control" id="MultiQCBrick" required value = "<?php echo $multiQC; ?>">
                            <small class="form-text text-muted">//TODO Add explication</small>
                        </div>

                        <div class="form-group">
                            <label for="" class="font-weight-bold">Sample :</label><br/>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="optradio" value="true">True
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="optradio" value = "false" checked>False
                                </label>
                            </div>
                        </div>

                        <br/>

                        <div class="form-group">
                            <label for="InstallBrick" class="font-weight-bold">Install:</label>
                            <textarea id="InstallBrick"  class="form-control" rows="5" maxlength="2000" required><?php echo $install; ?></textarea>
                            <small class="form-text text-muted">2000 characters maximum.</small>
                        </div>

                        <br/><br/>

                        <label for="" class="font-weight-bold">Action tool:</label><br/>


                        <button type="button" id="generateButton" class="btn btn-primary" onclick="generateBrick()">Generate</button>
                        <button type="button" id="downloadButton" class="btn btn-primary" onclick="downloadBrick()">Download</button>
                        <!-- <button type="button" id="saveButton" class="btn btn-primary">Save brick</button> -->

                        <?php


                            if(isset($_SESSION['username']) && !empty($_SESSION['username']))
                            {
                                echo '
                                <button type="button" class="btn btn-success" onclick="showFormTool(\'create\', \'\')">Publish</button>
                                <button type="button" class="btn btn-warning" onclick="showFormTool(\'update\', \''.$tags.'\')">Update</button>
                                ';

                            }
                        ?>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="col-12">
                <div class="card border border-dark">
                    <div class="card-header bg-dark text-light">
                        Commands :
                    </div>
                    <div class="card-body">
                        <div class="col-12 customInput">
                            <div class="col-12">
                                <div class="card border border-dark">
                                    <div class="card-header bg-menu2 text-light">
                                        Command parameters :
                                    </div>
                                    <div class="card-body fixHead form-row">

                                        <div class="form-group col-md-3">
                                            <label for="nameCommand">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                                            <input type="text" class="form-control" id="nameCommand">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="textCommand">Text <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                                            <input type="text" class="form-control" id="textCommand">
                                        </div>
                                        <!--<div class="form-group">
                                            <label for="commandCommand">Command <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                                            <input type="text" class="form-control" id="commandCommand">
                                        </div> -->
                                        <div class="form-group col-md-3">
                                            <label for="categoryCommand">Category<span style="font-size:20px;" class="status text-danger">*</span>:</label>
                                             <select class="form-control" name="categoryCommand" id="categoryCommand">
                                                <option value="other">Other</option>
                                                <?php  
                                                            
                                                foreach ($categories_title as $key => $value){
                                                    echo '<option value="'.$key.'">'.$value.'</option>';
                                                }
                                                ?>
                                    
                                            </select>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="commandCommand">How call your command <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                                            <input id="commandCommand"  class="form-control" required></input>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label for="inputsCommand">Inputs:</label>
                                            <table  class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Name</th>
                                                        <th>File</th>
                                                        <th>Description</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="inputsTable">
                                                </tbody>
                                            </table>

                                            <br/><br/>

                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th><label for="">Name :</label><input type="text" class="form-control" id="nameInputCommand"></th>
                                                        <th><label for="">File :</label><input type="text" class="form-control" id="fileInputCommand"></th>
                                                        <th><label for="">Description :</label><input type="text" class="form-control" id="descriptionInputCommand"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>

                                            <button type="button" class="btn btn-success" onclick="addInput()">Add input</button>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label for="outputsCommand">Outputs:</label>
                                            <table  class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Name</th>
                                                        <th>File</th>
                                                        <th>Description</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="outputsTable">
                                                </tbody>
                                            </table>

                                            <br/><br/>

                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th><label for="">Name :</label><input type="text" class="form-control" id="nameOutputCommand"></th>
                                                        <th><label for="">File :</label><input type="text" class="form-control" id="fileOutputCommand"></th>
                                                        <th><label for="">Description :</label><input type="text" class="form-control" id="descriptionOutputCommand"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>

                                            <button type="button" class="btn btn-success" onclick="addOutput()">Add output</button>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label for="outputDirCommand">Output Dir :</label>
                                            <input type="text" class="form-control" id="outputDirCommand">
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label for="optionCommand">Options :</label>
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Name</th>
                                                        <th>Type</th>
                                                        <th>Value</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="optionsTable">
                                                </tbody>
                                            </table>

                                            <br/><br/>

                                            <div class="form-group">
                                                <label for="" class="font-check-label">Options type :</label>
                                                <select class="custom-select" id="optionTypeSelect">
                                                    <option value="numeric">Numeric</option>
                                                    <option value="numericrange">Numeric Range</option>
                                                    <!--<option value="date">Date</option>
                                                    <option value="daterange">Date range</option>-->
                                                    <option value="text">Text</option>
                                                    <option value="select">Select</option>
                                                    <option value="file">File</option>
                                                    <option value="directory">Directory</option>
                                                    <option value="checkbox">Checkbox</option>
                                                    <option value="radio">Radio button</option>
                                                </select>
                                            </div>

                                            <button type="button" class="btn btn-success" onclick="showOptionModal()">Create option</button>

                                        </div>

            
                                        <button type="button" class="btn btn-primary" onclick="addCommand()">Add command</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/><br/>
                        <div class="col-12 customInput">
                            <div class="col-12">
                                <div class="card border border-dark">
                                    <div class="card-header bg-menu2 text-light">
                                        Command list :
                                    </div>
                                    <div class="card-body fixHead">

                                        <table id="optionsTable" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Name</th>
                                                        <th>Text</th>
                                                        <th>Category</th>
                                                        <th>Command</th>
                                                        <th>Inputs</th>
                                                        <th>Outputs</th>
                                                        <th>Output Dir</th>
                                                        <th>Options</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="commandsTable">
                                                </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/><br/>
    <div class="row">
        <div class="col-12">
            <div class="col-12">
                <div class="card border border-dark" id = "brickPreviewHead">
                    <div class="card-header bg-dark text-light">
                        Preview File :
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <textarea id="brickPreview"  class="form-control" rows="<?php echo $NROW; ?>"><?php echo $yaml;?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br/><br/><br/><br/>

    <div class="modal" tabindex="-1" role="dialog" id="numericModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Numeric option</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="nameNumericOption">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                    <input type="text" class="form-control" id="nameNumericOption">
                </div>
                <div class="form-group">
                    <label for="prefixNumericOption">Prefix :</label>
                    <input type="text" class="form-control" id="prefixNumericOption">
                </div>
                <div class="form-group">
                    <label for="valueNumericOption">Value :</label>
                    <input type="text" class="form-control" id="valueNumericOption">
                </div>
                <div class="form-group">
                    <label for="minNumericOption">Min :</label>
                    <input type="text" class="form-control" id="minNumericOption">
                </div>
                <div class="form-group">
                    <label for="maxNumericOption">Max :</label>
                    <input type="text" class="form-control" id="maxNumericOption">
                </div>
                <div class="form-group">
                    <label for="stepNumericOption">Step :</label>
                    <input type="text" class="form-control" id="stepNumericOption">
                </div>
                <div class="form-group">
                    <label for="labelNumericOption">Label :</label>
                    <input type="text" class="form-control" id="labelNumericOption">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick = "addNumeric()" data-dismiss="modal"> Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="numericrangeModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">NumericRange option</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="nameNumericRangeOption">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                    <input type="text" class="form-control" id="nameNumericRangeOption">
                </div>
                <div class="form-group">
                    <label for="prefixNumericRangeOption">Prefix :</label>
                    <input type="text" class="form-control" id="prefixNumericRangeOption">
                </div>
                <div class="form-group">
                    <label for="value1NumericRangeOption">Value 1:</label>
                    <input type="text" class="form-control" id="value1NumericRangeOption">
                </div>
                <div class="form-group">
                    <label for="value2NumericRangeOption">Value 2:</label>
                    <input type="text" class="form-control" id="value2NumericRangeOption">
                </div>
                <div class="form-group">
                    <label for="minNumericRangeOption">Min :</label>
                    <input type="text" class="form-control" id="minNumericRangeOption">
                </div>
                <div class="form-group">
                    <label for="maxNumericRangeOption">Max :</label>
                    <input type="text" class="form-control" id="maxNumericRangeOption">
                </div>
                <div class="form-group">
                    <label for="stepNumericRangeOption">Step :</label>
                    <input type="text" class="form-control" id="stepNumericRangeOption">
                </div>
                <div class="form-group">
                    <label for="labelNumericRangeOption">Label :</label>
                    <input type="text" class="form-control" id="labelNumericRangeOption">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick = "addNumericRange()" data-dismiss="modal">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="directoryModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Directory option</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="nameDirectoryOption">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                    <input type="text" class="form-control" id="nameDirectoryOption">
                </div>
                <div class="form-group">
                    <label for="" class="font-check-label">Type <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                    <select class="custom-select" id="typeDirectoryOption">
                        <option value="input_dir">input</option>
                        <option value="output_dir">output</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="prefixDirectoryOption">Prefix :</label>
                    <input type="text" class="form-control" id="prefixDirectoryOption">
                </div>
                <div class="form-group">
                    <label for="valueDirectoryOption">Value :</label>
                    <input type="text" class="form-control" id="valueDirectoryOption">
                </div>
                <div class="form-group">
                    <label for="labelDirectoryOption">Label :</label>
                    <input type="text" class="form-control" id="labelDirectoryOption">
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick = "addDirectory()" data-dismiss="modal">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="fileModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">File option</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="nameFileOption">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                    <input type="text" class="form-control" id="nameFileOption">
                </div>
                <div class="form-group">
                    <label for="" class="font-check-label">Type <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                    <select class="custom-select" id="typeFileOption">
                        <option value="input_file">input</option>
                        <option value="output_file">output</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="prefixFileOption">Prefix :</label>
                    <input type="text" class="form-control" id="prefixFileOption">
                </div>
                <div class="form-group">
                    <label for="valueFileOption">Value :</label>
                    <input type="text" class="form-control" id="valueFileOption">
                </div>
                <div class="form-group">
                    <label for="labelFileOption">Label :</label>
                    <input type="text" class="form-control" id="labelFileOption">
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick = "addFile()" data-dismiss="modal">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="textModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Text option</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="nameTextOption">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                    <input type="text" class="form-control" id="nameTextOption">
                </div>
                <div class="form-group">
                    <label for="prefixTextOption">Prefix :</label>
                    <input type="text" class="form-control" id="prefixTextOption">
                </div>
                <div class="form-group">
                    <label for="valueTextOption">Value :</label>
                    <input type="text" class="form-control" id="valueTextOption">
                </div>
                <div class="form-group">
                    <label for="labelTextOption">Label :</label>
                    <input type="text" class="form-control" id="labelTextOption">
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick = "addText()" data-dismiss="modal">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="checkboxModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Checkbox option</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="nameCheckboxOption">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                    <input type="text" class="form-control" id="nameCheckboxOption">
                </div>
                <div class="form-group">
                    <label for="prefixCheckboxOption">Prefix :</label>
                    <input type="text" class="form-control" id="prefixCheckboxOption">
                </div>
                <div class="form-group">
                    <label for="valueCheckboxOption">Value :</label>
                    <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" value="TRUE" id="value1CheckboxOption" name="valueCheckboxOption">checked
                    </label>
                    </div>
                    <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" value="FALSE" id="value2CheckboxOption" name="valueCheckboxOption" checked>not checked
                    </label>
                    </div>
                    
                </div>
                <div class="form-group">
                    <label for="labelCheckboxOption">Label :</label>
                    <input type="text" class="form-control" id="labelCheckboxOption">
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick = "addCheckbox()" data-dismiss="modal">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="radioModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Radio option</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="nameRadioOption">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                    <input type="text" class="form-control" id="nameRadioOption">
                </div>
                <div class="form-group">
                    <label for="prefixRadioOption">Prefix :</label>
                    <input type="text" class="form-control" id="prefixRadioOption">
                </div>
                <div class="form-group">
                    <label for="valueRadioOption">Value :</label>
                    <input type="text" class="form-control" id="valueRadioOption">
                </div>
                <div class="form-group">
                    <label for="choicesRadioOption">Choices :</label>
                    <table  class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody id="radiosTable">
                        </tbody>
                    </table>

                    <br/><br/>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th><label for="">Name :</label><input type="text" class="form-control" id="nameRadioCommand"></th>
                                <th><label for="">Value :</label><input type="text" class="form-control" id="valueRadioCommand"></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <button type="button" class="btn btn-success" onclick="addRadio()">Add choice</button>

                    
                </div>
                <div class="form-group">
                    <label for="labelRadioOption">Label :</label>
                    <input type="text" class="form-control" id="labelRadioOption">
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick = "addRadio2()" data-dismiss="modal">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="selectModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Select option</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="nameSelectOption">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                    <input type="text" class="form-control" id="nameSelectOption">
                </div>
                <div class="form-group">
                    <label for="prefixSelectOption">Prefix :</label>
                    <input type="text" class="form-control" id="prefixSelectOption">
                </div>
                <div class="form-group">
                    <label for="valueSelectOption">Value :</label>
                    <input type="text" class="form-control" id="valueSelectOption">
                </div>
                <div class="form-group">
                    <label for="choicesSelectOption">Choices :</label>
                    <table  class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody id="selectsTable">
                        </tbody>
                    </table>

                    <br/><br/>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th><label for="">Name :</label><input type="text" class="form-control" id="nameSelectCommand"></th>
                                <th><label for="">Value :</label><input type="text" class="form-control" id="valueSelectCommand"></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <button type="button" class="btn btn-success" onclick="addSelect()">Add choice</button>

                    
                </div>
                <div class="form-group">
                    <label for="labelSelectOption">Label :</label>
                    <input type="text" class="form-control" id="labelSelectOption">
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick = "addSelect2()" data-dismiss="modal">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

        <!-- Start Modal Publish -->
        <div class="modal" id="modalSaveTool">
            <div class="modal-dialog">
            <div class="modal-content">
            
                <!-- Modal Header -->
                <div class="modal-header">
                <h4 id="titleTool" class="modal-title">Publish tool</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="./action/action_tool.php" method="post">
                    <!-- Modal body -->
                    <div class="modal-body">
                        
                        <input id="actionTool" type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="create">
                        <div class="form-group">
                                <label for="tidTool">ID <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                                <input type="text" class="form-control" id="tidTool" name="tid" required>
                            </div>
                            <div class="form-group">
                                <label for="nameTool">Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                                <input type="text" class="form-control" id="nameTool" name="name" required>
                            </div>
                            <div class="form-group">
                                <label for="creationDateTool">Creation date :</label>
                                <input type="text" class="form-control" id="creationDateTool" name="creationDate" readonly="readonly">
                            </div>
                            <div class="form-group">
                                <label for="authorContainer">Author <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                                <input type="text" class="form-control" id="authorContainer" name="type" value="<?php echo $_SESSION['username'];?>" readonly>

                            </div>
                            <!--<div class="form-group">
                                <label for="visibilityTool">Visibility :</label>
                                <select class="form-control" id="visibilityTool" name="visibility">
                                    <option value="1">Public</option>
                                    <option value="0">Private</option>
                                </select>
                            </div>-->
                            <div class="form-group">
                                <label for="descriptionTool">Description <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                                <textarea class="form-control" rows="5" id="descriptionTool" maxlength="500" name ="description" required></textarea>
                            </div> 
                            <div class="form-group">
                                <label for="versionTool">Version <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                                <input type="text" class="form-control" id="versionTool" name="version" required>
                            </div>
                            <div class="form-group">
                                <label for="websiteTool">Website :</label>
                                <input type="text" class="form-control" id="websiteTool" name="website">
                            </div>
                            <div class="form-group">
                                <label for="gitTool">Git :</label>
                                <input type="text" class="form-control" id="gitTool" name="git">
                            </div>
                            <div class="form-group">
                                <label for="documentationTool">Documentation :</label>
                                <input type="text" class="form-control" id="documentationTool" name="documentation">
                            </div>
                            <div class="form-group">
                                <label for="articleTool">Article :</label>
                                <input type="text" class="form-control" id="articleTool" name="article">
                            </div>
                            <div class="form-group">
                                <label for="multiQCTool">MultiQC <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                                <input type="text" class="form-control" id="multiQCTool" name="multiQC">
                            </div>
                            <div class="form-group">
                                <label for="labelsTool">Labels :</label>
                                <select class="form-control" id="labelsTool" name="labels[]" multiple>
                                <?php
                                    foreach ($db->getLabelsWithGrade($db->getGradeWithLogin($_SESSION['username'])) as $label){

                                        if(in_array($label->name, $labels)) {
                                            echo '<option value="'.$label->ID.'" selected="selected">'.$label->name.'</option>';
                                        }
                                        else {
                                            echo '<option value="'.$label->ID.'">'.$label->name.'</option>';
                                        }

                                    }
                                ?>
                                </select>

                            </div>
                            <div class="form-group">
                                <label for="tagsTool">Tags :</label>
                                <textarea class="form-control" rows="5" id="tagsTool" maxlength="1000" name ="tags"><?php echo $tags ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="installTool">Install :</label>
                                <textarea type="text" rows="10" class="form-control" id="installTool" name="install" value=""></textarea>
                            </div>
                            <div class="form-group">
                                <label for="valueTool">YAML :</label>
                                <textarea type="text" rows="10" class="form-control" id="valueTool" name="yaml" value=""></textarea>
                            </div>
 
                    
                    </div>


                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </form>
                
            </div>
            </div>
        </div>  <!-- End Modal Publish -->

</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"  crossorigin="anonymous"></script>

<script src="./inc/js/brick.js"></script>

</body>
</html>
